const node_environment = process.env.NODE_ENV || 'development';

if (node_environment === 'development') {
  require('dotenv').config();
}

const db = require('./db/config');

const mongoose = require('mongoose');
mongoose.connect(db.uri, { useUnifiedTopology: true, useNewUrlParser: true });

const { manager } = require('./api/models/index');
const cryptography = require('./api/utils/cryptography.utils');

const createManager = async () => {
  await manager.create({
    email: 'juliafremder@gmail.com',
    name: 'Julia Fremder',
    password: cryptography.addHash('123456'),
  });
};

createManager();
