const node_environment = process.env.NODE_ENV || 'development';

if (node_environment === 'development') {
  require('dotenv').config();
}

const db = require('./db/config');

const mongoose = require('mongoose');
mongoose.connect(db.uri, { useUnifiedTopology: true, useNewUrlParser: true });

const { client } = require('./api/models/index');
const cryptography = require('./api/utils/cryptography.utils');

const createClient = async () => {
  await client.create({
    email: 'cliente@gmail.com',
    name: 'Client test one',
    password: cryptography.addHash('123456'),
  });
};

createClient();
