const db = require('../models/index');
const fileUtils = require('../utils/file.utils');
const autoserviceMapper = require('../mappers/autoservice.mapper');
const ErrorBusinessRule = require('../utils/errors/err-business-rule');

const AddNewAutoservice = async (model) => {
  const { category_id, supplier_id, image } = model;

  const [categoryDB, supplierDB] = await Promise.all([
    db.category.findById(category_id),
    db.supplier.findById(`${supplier_id}`),
  ]);
  //TODO: check supplier existence
  if (!supplierDB) {
    throw new ErrorBusinessRule('Não existe fornecedor com esse id.')
    // return {
    //   success: false,
    //   message: 'Não existe fornecedor com esse id.',
    //   data: 'não há dados a serem retornados',
    // };
  }
  //TODO: check category existence
  if (!categoryDB) {
    throw new ErrorBusinessRule('Não existe categoria com esse id.')
    // return {
    //   success: false,
    //   message: 'Não existe categoria com esse id.',
    //   data: 'não há dados a serem retornados',
    // };
  }

  //TODO: check if this autoservice existence

  //TODO: create the newAutoservice
  const newAutoservice = await db.autoservice.create({
    name: model.name,
    description: model.description,
    cost: Number(model.cost),
    category: model.category_id,
    supplier: model.supplier_id,
    image: {
      oldName: model.image.name,
      name: model.image.updatedName,
      type: model.image.type,
    },
  });

  //TODO: add new autoservice to the rigth category
  categoryDB.autoservice_id = [
    ...categoryDB.autoservice_id,
    newAutoservice._id,
  ];

  //TODO: add new autoservice to the rigth supplier
  supplierDB.autoservice_id = [
    ...supplierDB.autoservice_id,
    newAutoservice._id,
  ];

  await Promise.all([categoryDB.save(), supplierDB.save()]);

  fileUtils.move(image.path, image.updatedPath);
  return {
    success: true,
    mensage: 'cadastro realizado com sucesso',
    data: newAutoservice,
  };
};

const deleteAutoservice = async (supplier_id, autoservice_id, user_id ) => {
  const supplierDB = await db.supplier.findById(supplier_id)
  const autoserviceDB = await db.autoservice.findById(autoservice_id);

  if (!supplierDB) {
    throw new ErrorBusinessRule('Não existe fornecedor com esse id.')
    // return {
    //   success: false,
    //   message: 'Não existe fornecedor com esse id.',
    //   data: 'não há dados a serem retornados',
    // };
  }

  //is the user the same as the supplier?
  if (`${supplier_id}` !== `${user_id}`) {
    throw new ErrorBusinessRule('O serviço a ser excluído pertence a outro fornecedor.')
    // return {
    //   success: false,
    //   message: 'Somente o próprio fornecedor pode excluir seus serviços.',
    //   details: ['O serviço a ser excluído pertence a outro fornecedor.'],
    // };
  }

  if (!autoserviceDB) {
    throw new ErrorBusinessRule('O serviço a ser excluído não existe na base de dados.')
    // return {
    //   success: false,
    //   message: 'Não encontramos serviço com o id fornecido.',
    //   details: ['O serviço a ser excluído não existe na base de dados.'],
    //};
  }

  //is this service of this supplier?
  if (`${autoserviceDB.supplier}` !== `${supplierDB._id}`) {
      throw new ErrorBusinessRule('Esse serviço não pertence a esse fornecedor.')
      //return {
      // success: false,
      // message: 'Operação não pode ser realizada',
      // details: ['Esse serviço não pertence a esse fornecedor.'],
    //};
  }

  //search category and remove autoservice
  const categoryDB = await db.category.findById(autoserviceDB.category);
  categoryDB.autoservice_id = categoryDB.autoservice_id.filter((item) => {
    return item.toString() !== autoservice_id;
  });

  //TODO: remover produto do fornecedor
  supplierDB.autoservice_id = supplierDB.autoservice_id.filter((item) => {
    return item.toString() !== autoservice_id.toString();
  });

  //TODO: excluir do produto da base
  await Promise.all([
    categoryDB.save(),
    supplierDB.save(),
    db.autoservice.deleteOne(autoserviceDB),
  ]);

  return {
    success: true,
    message: 'Exclusão realizada com sucesso',
    data: { id_excluded: autoservice_id, service_excluded: autoserviceDB.name },
  };
};

const filterAutoservice = async (filters) => {
  try {
    const filterMongo = {};

    if (filters.category) {
      filterMongo.category = filters.category;
    }

    if (filters.supplier) {
      filterMongo.supplier = filters.supplier;
    }

    if (filters.nameLike) {
      filterMongo.name = { $regex: '.*' + filters.namelike + '.*' };
    }

    const resultDB = await db.autoservice
      .find(filterMongo)
      .populate('category_id');

    const data = resultDB.map((item) =>
      autoserviceMapper.toListAutoservicesDTO(item),
    );

    return {
      success: true,
      message: 'serviços cadastrados',
      data: data,
    };
  } catch (error) {
    // console.log(error, 'error no filter Mongo');
  }
};

module.exports = {
  AddNewAutoservice,
  filterAutoservice,
  deleteAutoservice,
};
