const db = require('../models/index');
const { checkEmail } = require('./user.service');
const { userDTO } = require('../mappers/user.mapper');
const { addHash } = require('../utils/cryptography.utils');

const ErrorBusinessRule = require('../utils/errors/err-business-rule')

const addClient = async (model) => {
  const { email, password, ...rest } = model;

  //TODO: check if e-mail exists
  if (await checkEmail(email)){
    throw new ErrorBusinessRule('Já existe usuário cadastrado para o email informado')
  }
    // return {
    //   success: false,
    //   message: 'operação não pode ser realizada',
    //   details: ['Já existe usuário cadastrado para o email informado'],
    // };
  const newClient = await db.client.create({
    email,
    ...rest,
    password: addHash(password),
  });
  return {
    success: true,
    message: 'Operação realizada com sucesso',
    data: {
      newClient,
    },
  };
};

module.exports = {
  addClient,
};
