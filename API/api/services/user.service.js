const { user } = require('../models/index');
const cryptography = require('../utils/cryptography.utils');
const userMapper = require('../mappers/user.mapper');
const ErrorUserNotAuthenticated = require ('../utils/errors/err-user-not-authent')
const ErrorUserNotAuthorised = require ('../utils/errors/err-user-not-authorize')

const userIsValid = async (email, password) => {
  const resultDB = await user.findOne({
    email,
    password: cryptography.addHash(password),
  });
  return resultDB;
};

const profiles = [
  {
    id: '1',
    description: 'Manager',
    permitions: [
      'ADD_CATEGORY',
      'APPROVE_SUPPLIER',
      'REFUSE_SUPPLIER',
      'CATEGORY_DETAILS',
      'UPDATE_CATEGORY',
      'DELETE_CATEGORY',
      'SUPPLIER_DETAILS',
    ],
  },
  {
    id: '2',
    description: 'Supplier',
    permitions: ['DELETE_AUTOSERVICE', 'SUPPLIER_DETAILS', 'ADD-AUTOSERVICE'],
  },
  {
    id: '3',
    description: 'Client',
    permitions: ['ADD_LIKE', 'DELETE_LIKE', 'CLIENT_DETAILS', 'LIKES_DETAILS'],
  },
];

const findProfileByDescription = (description) => {
  const result = profiles.find((item) => item.description === description);
  return result;
};

const checkPermitionsByProfile = (kind, permition) => {
  const profile = findProfileByDescription(kind);
  return profile.permitions.includes(permition);
};

const addCredential = async (userEmail) => {
  const userDB = await user.findOne({
    email: userEmail,
  });

  const userServiceDTO = userMapper.userDTO(userDB);
  const token = cryptography.addToken(userServiceDTO);
  return {
    token,
    userServiceDTO,
  };
};

const authenticate = async (email, password) => {
  const resultDB = await userIsValid(email, password);

  if (!resultDB) {
    throw new ErrorUserNotAuthenticated('E-mail de utilizador ou palavra-passe estão errados.')
    // return {
    //   success: false,
    //   message: 'Não foi possível autenticar o usuário.',
    //   details: ['E-mail ou password estão errados.'],
    // };
  }

  if (resultDB.status === 'Refused') {
    throw new ErrorUserNotAuthorised('Fornecedor BLOQUEADO')
    // return {
    //   success: false,
    //   message: 'Fornecedor BLOQUEADO.',
    //   details: [{ userServiceDTO: { status: 'Refused' } }],
    // };
  }
  if (resultDB.status === 'Pending') {
    throw new ErrorUserNotAuthenticated('Fornecedor aguardando aprovação pelo administrador do sistema.')
    // return {
    //   success: false,
    //   message: 'Fornecedor aguardando aprovação',
    //   details: [{ userServiceDTO: { status: 'Pending' } }],
    // };
  }

  return {
    success: true,
    data: await addCredential(email),
    message: 'Usuário autenticado.',
  };
};

const checkEmail = async (email) => {
  const emailDuplicated = await user.findOne({ email });
  return emailDuplicated;
};

const deleteUser = async (id) => {
  //TODO: localizar documento
  const userDB = await user.findById(id);

  if (!userDB) {
    return {
      success: false,
      message: 'Não foi possível excluir o usuario',
      details: ['Não há usuarios com esse id na base de dados.'],
    };
  }

  //TODO: criar tratamento para quando existem serviços associados ao fornecedor

  //confere se é administrador
  if (userDB.kind === 'manager') {
    return {
      success: false,
      message: 'Não foi possível excluir o administrador',
      details: ['Administradores não podem ser removidos.'],
    };
  }
  //TODO: deleta do banco
  await user.deleteOne({
    _id: id,
  });

  return {
    success: true,
    message: 'Operação realizada com sucesso.',
    details: 'Fornecedor excluído.',
  };
};

module.exports = {
  authenticate,
  checkEmail,
  deleteUser,
  checkPermitionsByProfile,
};
