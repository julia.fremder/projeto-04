const { supplier } = require('../models/index');
const { checkEmail } = require('./user.service');
const { addHash } = require('../utils/cryptography.utils')
const db = require('../models');
const userMapper = require('../mappers/user.mapper');
const fileUtils = require('../utils/file.utils');
const emailUtils = require('../utils/email.utils')
const ErrorBusinessRule = require('../utils/errors/err-business-rule')

const findItemById = async (supplier_id) => {
  const supplierDB = await supplier.findById(supplier_id);

  if(!supplierDB){
    throw new ErrorBusinessRule('Não há fornecedor com esse id na base de dados.')
  }

  if (supplierDB){
    return {
      success: true,
      message: 'Fornecedor encontrado.',
      data: userMapper.userDTO (supplierDB)
    }
  }

};


const checkNIPC = async (NIPC) => {
  const nipcDuplicated = await db.user.findOne({ NIPC })
  return nipcDuplicated
}


const addSupplier = async (model) => {
  const { email, NIPC, password, name, street, zipcode,phone, image } = model;

  if (await checkEmail(email)) {
    throw new ErrorBusinessRule('Já existe empresa cadastrada com esse e-mail.')
    // return {
    //   success: false,
    //   message: 'A operação falhou',
    //   details: ['Já existe empresa cadastrada com esse email.'],
    // };
  }

  if (await checkNIPC(NIPC)) {
    throw new ErrorBusinessRule('Já existe empresa cadastrada com esse NIPC.')
    // return {
    //   success: false,
    //   message: 'A operação falhou',
    //   details: ['Já existe empresa cadastrada com esse NIPC.'],
    // };
  }

  const newSupplier = await supplier.create({
    email,
    NIPC,
    name,
    password: addHash(password),
    zipcode,
    street,
    phone,
    status: 'Pending',
    image: {
      oldName: image.name,
      name: image.updatedName,
      type: image.type,
    },
  });

  fileUtils.move(image.path, image.updatedPath);

  return {
    success: true,
    message: 'Cadastro de fornecedor realizado com sucesso.',
    data: userMapper.userDTO(newSupplier._doc),
  };
};

const updateSupplier = async (supplier_id, model) => {
  const supplierDB = await supplier.findById(supplier_id);

  if (!supplierDB) {
    throw new ErrorBusinessRule('Não há user com esse id na base de dados.')
    // return {
    //   success: false,
    //   message: 'Não foi possível atualizar o User.',
    //   details: ['Não há user com o id informado.'],
    // };
  }

  supplierDB.companyName = model.companyName;
  supplierDB.NIPC = model.NIPC;
  supplierDB.email = model.email;
  supplierDB.status = supplierDB.status;

  await supplierDB.save();

  return {
    success: true,
    message: 'Atualização concluída',
    data: userMapper.userDTO(supplierDB),
  };
};

const updateStatusSupplier = async (supplier_id, status) => {

  const supplierDB = await supplier.findById(supplier_id);

  if (!supplierDB) {
    throw new ErrorBusinessRule('Não há fornecedor com esse id na base de dados.')
    // return {
    //   success: false,
    //   message: 'Operação não realizada.',
    //   details: ['Não há fornecedor com esse id na base de dados.'],
    // };
  } else {
    supplierDB.status = status;

    await supplierDB.save();

    if(status === 'Approved') {
      //send mail to user.mail
      emailUtils.send({
        recipient: supplierDB.email,
        from: process.env.SG_FROM,
        subject: `confirmação`,
        text: `a sua conta esta liberada`
      })

    }

    return {
      success: true,
      message: 'Atualização concluída',
      data: userMapper.userDTO(supplierDB),
    };
  }
};

const findAll = async () => {
  const listSuppliersDB = await supplier.find({});

  return listSuppliersDB.map((supplierDB) => {
    return userMapper.userDTO(supplierDB);
  });
};
module.exports = {
  addSupplier,
  updateSupplier,
  updateStatusSupplier,
  findAll,
  findItemById
};
