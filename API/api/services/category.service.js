const { category } = require('../models/index');
const categoryMapper = require('../mappers/category.mapper');
const fileUtils = require('../utils/file.utils');
const ErrorBusinessRule = require('../utils/errors/err-business-rule');

const findItemById = async (category_id) => {
  const categoryDB = await category.findById(category_id);

  if (categoryDB) {
    return {
      success: true,
      message: 'Categoria encontrada.',
      data: categoryMapper.toDTO(categoryDB),
    };
  }
};

const findAll = async () => {
  const listCategoriesDB = await category.find({});

  return listCategoriesDB.map((categoryDB) => {
    return categoryMapper.toListDTO(categoryDB);
  });
};

const checkCategory = async (name) => {
  const categoryDuplicated = await category.findOne({ name });
  return categoryDuplicated;
};

const addCategory = async (model) => {
  //TODO: tratar nomes repetidos

  if (await checkCategory(model.name)) {
    throw new ErrorBusinessRule(
      'Já existe essa categoria cadastrada na base de dados.',
    );
    // return {
    //   success: false,
    //   message: 'Operação falhou.',
    //   details: ['Já existe essa categoria cadastrada na base de dados.'],
    // };
  }
  //   //TODO: incluir nova categoria

  const newCategory = await category.create({
    name: model.name,
    description: model.description,
    status: model.status,
    image: {
      oldName: model.image.name,
      name: model.image.updatedName,
      type: model.image.type,
    },
  });

  //   //TODO: mover arquivo para destino definitivo
  fileUtils.move(model.image.path, model.image.updatedPath);

  return {
    success: true,
    message: 'cadastro de categoria realizado com sucesso',
    data: categoryMapper.toDTO(newCategory),
  };
};

const updateItem = async (category_id, model) => {
  const categoryDB = await category.findById(category_id);

  if (!categoryDB) {
    throw new ErrorBusinessRule('Não há categoria com o ID informado.');
    // return {
    //   success: false,
    //   message: 'Não foi possível atualizar o item.',
    //   details: ['Não há um item com o id informado.'],
    // };
  }

  categoryDB.name = model.name;
  categoryDB.description = model.description;
  categoryDB.status = model.status;

  if (model.image) {
    fileUtils.remove('categories', categoryDB.image.name);
    fileUtils.move(model.image.path, model.image.updatedPath);
    categoryDB.image = {
      oldName: model.image.name,
      name: model.image.updatedName,
      type: model.image.type,
    };
  }

  await categoryDB.save();

  return {
    success: true,
    message: 'Atualização concluída',
    data: categoryMapper.toDTO(categoryDB),
  };
};

const deleteItem = async (category_id) => {
  //TODO: localizar documento
  const categoryDB = await category.findOne({ _id: category_id });

  if (!categoryDB) {
    throw new ErrorBusinessRule(
      'Não há categoria com esse ID na base de dados.',
    );
    // return {
    //   success: false,
    //   message: 'Não foi possível excluir a categoria',
    //   details: ['Não há categoria com esse id na base de dados.'],
    // };
  }

  //TODO: criar tratamento para quando existem produtos associados a categoria

  //TODO: destruir a imagem
  const { image } = categoryDB;
  fileUtils.remove('categories', image.name);

  //TODO: deleta do banco
  await category.deleteOne({
    _id: category_id,
  });

  return {
    success: true,
    message: 'Operação de exclusão realizada com sucesso.',
    details: ['categoria removida'],
  };
};

module.exports = {
  findItemById,
  findAll,
  deleteItem,
  addCategory,
  updateItem,
};
