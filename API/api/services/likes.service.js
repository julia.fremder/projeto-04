const db = require('../models/index')

const addLike = async (supplier_id, user_id) => {

  const [supplierDB, clientDB] = await Promise.all([
    db.supplier.findById(supplier_id),
    db.client.findById(user_id),
  ]);



  //TODO: remover validacao para uma camada separada e reaproveitar

  if (!supplierDB) {
    return {
      success: false,
      message: "operação não pode ser realizada",
      details: [
        "o fornecedor pesquisado não existe"
      ]
    }
  }

  const curtidaDB = await curtida.create({
    fornecedor: fornecedorid,
    cliente: usuarioid,
  });

  fornecedorDB.curtidas = [...fornecedorDB.curtidas, curtidaDB._id];
  clienteDB.curtidas = [...clienteDB.curtidas, curtidaDB._id];

  await Promise.all([
    fornecedorDB.save(),
    clienteDB.save()
  ]);

  return {
    success: true,
    message: 'operação realizada',
    data: {
      id: curtidaDB._id,
      fornecedor: fornecedorDB.nomeFantasia,
      cliente: clienteDB.nome,
    }
  }

};


const remove = async (fornecedorid, usuarioid) => {

  const [fornecedorDB, usuarioDB, curtidaDB] = await Promise
    .all([
      fornecedor.findById(fornecedorid),
      cliente.findById(usuarioid),
      curtida.findOne({ fornecedor: fornecedorid, cliente: usuarioid }),
    ]);

  if (!fornecedorDB) {
    return {
      success: false,
      message: "operação não pode ser realizada",
      details: [
        "o fornecedor informado não existe"
      ]
    }
  }

  if (!curtidaDB) {
    return {
      success: false,
      message: "operação não pode ser realizada",
      detalhes: [
        "não existem curtidas para os dados informados"
      ]
    }
  }

  fornecedorDB.curtidas = fornecedorDB.curtidas.filter(item => {
    return item.toString() !== curtidaDB._id.toString();
  })

  const curtida_id = curtidaDB._id.toString();

  usuarioDB.curtidas = usuarioDB.curtidas.filter(item => {
    return item.toString() !== curtidaDB._id.toString()
  });

  await Promise.all([
    fornecedorDB.save(),
    usuarioDB.save(),
    curtida.remove(curtidaDB),
  ]);

  return {
    success: false,
    message: "operação realizada com sucesso",
    data: {
      id: curtida_id
    }
  }

}


module.exports = {
  cria,
  remove,
}