const { Router } = require('express');
const { name, version } = require('../../package.json');

const routesUserV1 = require('./v1/user');
const routesCategoryV1 = require('./v1/category');
const routesSupplierV1 = require('./v1/supplier');
const routesClientV1 = require('./v1/client')
const routesAutoservicesV1 = require('./v1/autoservice')

module.exports = (app) => {
  app.get('/', (req, res) => {
    res.send({ name, version });
  });

  const routerV1 = Router();

  routesUserV1(routerV1);
  routesSupplierV1(routerV1);
  routesCategoryV1(routerV1);
  routesClientV1(routerV1)
  routesAutoservicesV1(routerV1)

  app.use('/v1', routerV1);
};
