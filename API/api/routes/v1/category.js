const joi = require('joi');
const validateDTO = require('../../utils/middlewares/validate-dto.middleware');
const fileUploadMiddleware = require('../../utils/middlewares/file-upload.middleware');
const categoryController = require('../../controllers/v1/category.controller');
const authorizeMiddleware = require('../../utils/middlewares/authorization.middleware');
const asyncMiddleware = require('../../utils/middlewares/async-middleware');

module.exports = (router) => {
  router
    .route('/category')
    .get(asyncMiddleware(categoryController.listCategories))
    .post(
      asyncMiddleware(authorizeMiddleware('ADD_CATEGORY')),
      asyncMiddleware(fileUploadMiddleware('categories')),
      asyncMiddleware(
        validateDTO(
          'body',
          {
            name: joi.string().required().messages({
              'any.required': `Nome é um campo obrigatório`,
              'string.empty': `Nome não deve ser vazio`,
            }),
            description: joi.string().required().messages({
              'any.required': `Descrição é um campo obrigatório`,
              'string.empty': `Descrição não deve ser vazio`,
            }),
            status: joi.boolean().required().messages({
              'any.required': `Status é um campo obrigatório`,
              'booleam.empty': `Status não deve ser vazio`,
            }),
          },
          {
            allowUnknown: true,
          },
        ),
      ),
      asyncMiddleware(categoryController.addCategory),
    );

  router
    .route('/category/:category_id')
    .get(
      asyncMiddleware(authorizeMiddleware('CATEGORY_DETAILS')),
      asyncMiddleware(
        validateDTO('params', {
          category_id: joi
            .string()
            .regex(/^[0-9a-fA-F]{24}$/)
            .required()
            .messages({
              'any.required': `O id da categoria é um campo obrigatório`,
              'string.empty': `O id da categoria não deve ser vazio`,
              'string.regex': `O id da categoria está fora do formato experado`,
            }),
        }),
      ),
      asyncMiddleware(categoryController.findCategory),
    )
    .delete(
      asyncMiddleware(authorizeMiddleware('DELETE_CATEGORY')),
      asyncMiddleware(
        validateDTO('params', {
          category_id: joi
            .string()
            .regex(/^[0-9a-fA-F]{24}$/)
            .required()
            .messages({
              'any.required': `O id da categoria é um campo obrigatório`,
              'string.empty': `O id da categoria não deve ser vazio`,
              'string.regex': `O id da categoria está fora do formato experado`,
            }),
        }),
      ),
      categoryController.deleteCategory,
    )
    .put(
      asyncMiddleware(fileUploadMiddleware('categories', true)),
      asyncMiddleware(
        validateDTO('params', {
          category_id: joi.string().required().messages({
            'any.required': `O id da categoria é um campo obrigatório`,
            'string.empty': `O id da categoria não deve ser vazio`,
            'string.regex': `O id da categoria está fora do formato experado`,
          }),
        }),
        validateDTO(
          'body',
          {
            name: joi.string().required().messages({
              'any.required': `Nome é um campo obrigatório`,
              'string.empty': `Nome não deve ser vazio`,
            }),
            description: joi.string().required().messages({
              'any.required': `Descrição é um campo obrigatório`,
              'string.empty': `Descrição não deve ser vazio`,
            }),
            status: joi.boolean().required().messages({
              'any.required': `Status é um campo obrigatório`,
              'booleam.empty': `Status não deve ser vazio`,
            }),
          },
          {
            allowUnknown: true,
          },
        ),
      ),
      categoryController.updateCategory,
    );
};
