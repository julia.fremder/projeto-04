const joi = require('joi');
const validateDTO = require('../../utils/middlewares/validate-dto.middleware');
const clientController = require('../../controllers/v1/client.controller');
const fileUploadMiddleware = require('../../utils/middlewares/file-upload.middleware');
const asyncMiddleware = require('../../utils/middlewares/async-middleware');
const authorizeMiddleware = require('../../utils/middlewares/authorization.middleware');

module.exports = (router) => {
  router.route('/client').post(
    asyncMiddleware(fileUploadMiddleware('suppliers')),
    asyncMiddleware(
      validateDTO(
        'body',
        {
          name: joi.string().required().messages({
            'any.required': 'Nome do cliente é obrigatório',
            'string.empty': 'E-mail não pode estar vazio',
          }),
          NIF: joi.string().length(9).required().messages({
            'any.required': 'Número fiscal do cliente é obrigatório',
            'string.empty': 'E-mail não pode estar vazio',
          }),
          email: joi.string().required().messages({
            'any.required': 'E-mail é obrigatório',
            'string.empty': 'E-mail não pode estar vazio',
          }),
          password: joi.string().required().messages({
            'any.required': 'Senha é obrigatório.',
            'string.empty': 'Senha não pode estar vazio.',
          }),
          phone: joi.string().required().messages({
            'any.required': 'Telefone é obrigatório.',
            'string.empty': 'Telefone não pode estar vazio.',
          }),
          zipcode: joi.string().required().messages({
            'any.required': 'codigo postal é obrigatório',
            'string.empty': 'codigo postal não pode estar vazio',
          }),
          street: joi.string().required().messages({
            'any.required': 'Morada é obrigatório',
            'string.empty': 'Morada não pode estar vazio',
          }),
        },
        {
          allowUnknown: true,
        },
      ),
    ),
    clientController.addClient,
  );

  router.route('/client/:client_id').get(
    asyncMiddleware(authorizeMiddleware('CLIENT_DETAILS')),
    asyncMiddleware(
      validateDTO('params', {
        client_id: joi
          .string()
          .regex(/^[0-9a-fA-F]{24}$/)
          .required()
          .messages({
            'any.required': `"cliente id" é um campo obrigatório`,
            'string.empty': `"cliente id" não deve ser vazio`,
            'string.pattern.base': `"cliente id" fora do formato experado`,
          }),
      }),
    ),
    clientController.findClientById,
  );

  router.route('/client/:client_id/likes').get(
    asyncMiddleware(authorizeMiddleware('LIKES_DETAILS')),
    asyncMiddleware(
      validateDTO('params', {
        client_id: joi
          .string()
          .regex(/^[0-9a-fA-F]{24}$/)
          .required()
          .messages({
            'any.required': `"cliente id" é um campo obrigatório`,
            'string.empty': `"cliente id" não deve ser vazio`,
            'string.pattern.base': `"cliente id" fora do formato experado`,
          }),
      }),
    ),
    clientController.seeLikes,
  );
};
