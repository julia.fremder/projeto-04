const userController = require('../../controllers/v1/user.controller');
const validateDTO = require('../../utils/middlewares/validate-dto.middleware');

const joi = require('joi');
const asyncMiddleware = require('../../utils/middlewares/async-middleware');

module.exports = (router) => {
  router.route('/auth').post(
    asyncMiddleware(
      validateDTO('body', {
        email: joi.string().required().messages({
          'any.required': 'E-mail é obrigatório',
          'string.empty': 'E-mail não pode estar vazio',
        }),
        password: joi.string().required().messages({
          'any.required': 'Senha é obrigatório.',
          'string.empty': 'Senha não pode estar vazio.',
        }),
      }),
    ),
    asyncMiddleware(userController.auth),
  );
};
