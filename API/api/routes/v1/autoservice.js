const joi = require('joi');
const validateDTO = require('../../utils/middlewares/validate-dto.middleware');
const autoserviceController = require('../../controllers/v1/autoservices.controller');
const asyncMiddleware = require('../../utils/middlewares/async-middleware');

module.exports = (router) => {
  router.route('/autoservice').get(
    asyncMiddleware(
      validateDTO('query', {
        category: joi
          .string()
          .regex(/^[0-9a-fA-F]{24}$/)
          .messages({
            'any.required': `"categoria id" é um campo obrigatório`,
            'string.empty': `"categoria id" não deve ser vazio`,
            'string.pattern.base': `"categoria id" fora do formato experado`,
          }),
        supplier: joi
          .string()
          .regex(/^[0-9a-fA-F]{24}$/)
          .messages({
            'any.required': `"fornecedor id" é um campo obrigatório`,
            'string.empty': `"fornecedor id" não deve ser vazio`,
            'string.pattern.base': `"fornecedor id" fora do formato experado`,
          }),
        namelike: joi.string(),
      }),
    ),

    asyncMiddleware(autoserviceController.getListAutoservices),
  );
};
