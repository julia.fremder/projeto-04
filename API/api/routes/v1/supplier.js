const joi = require('joi');
const supplierController = require('../../controllers/v1/supplier.controller');
const fileUploadMiddleware = require('../../utils/middlewares/file-upload.middleware');
const authorizeMiddleware = require('../../utils/middlewares/authorization.middleware');
const validateDTO = require('../../utils/middlewares/validate-dto.middleware');
const autoservicesController = require('../../controllers/v1/autoservices.controller');
const asyncMiddleware = require('../../utils/middlewares/async-middleware');

module.exports = (router) => {
  router
    .route('/supplier')
    .get(asyncMiddleware(supplierController.listSuppliers))
    .post(
      asyncMiddleware(fileUploadMiddleware('suppliers')),
      asyncMiddleware(
        validateDTO(
          'body',
          {
            name: joi.string().required().messages({
              'any.required': 'Nome da Empresa é obrigatório',
              'string.empty': 'E-mail não pode estar vazio',
            }),
            NIPC: joi.string().length(9).required().messages({
              'any.required': 'Número fiscal da empresa é obrigatório',
              'string.empty': 'E-mail não pode estar vazio',
            }),
            email: joi.string().required().messages({
              'any.required': 'E-mail é obrigatório',
              'string.empty': 'E-mail não pode estar vazio',
            }),
            password: joi.string().required().messages({
              'any.required': 'Senha é obrigatório.',
              'string.empty': 'Senha não pode estar vazio.',
            }),
            phone: joi.string().required().messages({
              'any.required': 'Telefone é obrigatório.',
              'string.empty': 'Telefone não pode estar vazio.',
            }),
            zipcode: joi.string().required().messages({
              'any.required': 'codigo postal é obrigatório',
              'string.empty': 'codigo postal não pode estar vazio',
            }),
            street: joi.string().required().messages({
              'any.required': 'Morada é obrigatório',
              'string.empty': 'Morada não pode estar vazio',
            }),
          },
          {
            allowUnknown: true,
          },
        ),
      ),
      asyncMiddleware(supplierController.newSupplier),
    );

  router
    .route('/supplier/:supplier_id')
    .get(
      asyncMiddleware(authorizeMiddleware('SUPPLIER_DETAILS')),
      asyncMiddleware(
        validateDTO('params', {
          supplier_id: joi
            .string()
            .regex(/^[0-9a-fA-F]{24}$/)
            .required()
            .messages({
              'any.required': `O id do fornecedor é um campo obrigatório`,
              'string.empty': `O id do fornecedor não deve ser vazio`,
              'string.regex': `O id do fornecedor está fora do formato experado`,
            }),
        }),
      ),
      asyncMiddleware(supplierController.findSupplier),
    )
    .put(
      asyncMiddleware(fileUploadMiddleware('suppliers', true)),
      asyncMiddleware(
        validateDTO('params', {
          supplier_id: joi
            .string()
            .regex(/^[0-9a-fA-F]{24}$/)
            .required()
            .messages({
              'any.required': `O id do Fornecedor é um campo obrigatório`,
              'string.empty': `O id do Fornecedor não deve ser vazio`,
              'string.regex': `O id do Fornecedor está fora do formato experado`,
            }),
        }),
        validateDTO('body', {
          name: joi.string().required().messages({
            'any.required': 'Nome da Empresa é obrigatório',
            'string.empty': 'E-mail não pode estar vazio',
          }),
          NIPC: joi.string().length(9).required().messages({
            'any.required': 'Número fiscal da empresa é obrigatório',
            'string.empty': 'E-mail não pode estar vazio',
          }),
          email: joi.string().required().messages({
            'any.required': 'E-mail é obrigatório',
            'string.empty': 'E-mail não pode estar vazio',
          }),
        }),
      ),
      asyncMiddleware(supplierController.updateSupplier),
    );

  router.route('/supplier/:supplier_id/approve').put(
    asyncMiddleware(authorizeMiddleware('APPROVE_SUPPLIER')),
    asyncMiddleware(
      validateDTO('params', {
        supplier_id: joi
          .string()
          .regex(/^[0-9a-fA-F]{24}$/)
          .required()
          .messages({
            'any.required': `O id do Fornecedor é um campo obrigatório`,
            'string.empty': `O id do Fornecedor não deve ser vazio`,
            'string.regex': `O id do Fornecedor está fora do formato experado`,
          }),
      }),
    ),
    asyncMiddleware(supplierController.approveSupplier),
  );
  router.route('/supplier/:supplier_id/refuse').put(
    asyncMiddleware(authorizeMiddleware('REFUSE_SUPPLIER')),
    asyncMiddleware(
      validateDTO('params', {
        supplier_id: joi
          .string()
          .regex(/^[0-9a-fA-F]{24}$/)
          .required()
          .messages({
            'any.required': `O id do Fornecedor é um campo obrigatório`,
            'string.empty': `O id do Fornecedor não deve ser vazio`,
            'string.regex': `O id do Fornecedor está fora do formato experado`,
          }),
      }),
    ),
    asyncMiddleware(supplierController.refuseSupplier),
  );

  router
    .route('/supplier/:supplier_id/autoservices')
    .get(asyncMiddleware(autoservicesController.getListAutoservices))
    .post(
      asyncMiddleware(authorizeMiddleware('ADD-AUTOSERVICE')),
      asyncMiddleware(fileUploadMiddleware('autoservices')),
      asyncMiddleware(
        validateDTO('params', {
          supplier_id: joi
            .string()
            .regex(/^[0-9a-fA-F]{24}$/)
            .required()
            .messages({
              'any.required': `"fornecedor id" é um campo obrigatório`,
              'string.empty': `"fornecedor id" não deve ser vazio`,
              'string.pattern.base': `"fornecedor id" fora do formato experado`,
            }),
        }),
        validateDTO(
          'body',
          {
            name: joi.string().required().messages({
              'any.required': `"nome" é um campo obrigatório`,
              'string.empty': `"nome" não deve ser vazio`,
            }),
            description: joi.string().required().messages({
              'any.required': `"descricao" é um campo obrigatório`,
              'string.empty': `"descricao" não deve ser vazio`,
            }),
            category_id: joi
              .string()
              .regex(/^[0-9a-fA-F]{24}$/)
              .required()
              .messages({
                'any.required': `"categoria id" é um campo obrigatório`,
                'string.empty': `"categoria id" não deve ser vazio`,
                'string.pattern.base': `"categoria id" fora do formato experado`,
              }),
            cost: joi.number().required().messages({
              'any.required': `"preco" é um campo obrigatório`,
            }),
          },
          {
            allowUnknown: true,
          },
        ),
      ),
      asyncMiddleware(autoservicesController.addAutoservices),
    );

  router.route('/supplier/:supplier_id/autoservices/:autoservice_id').delete(
    asyncMiddleware(authorizeMiddleware('DELETE_AUTOSERVICE')),
    asyncMiddleware(
      validateDTO('params', {
        supplier_id: joi
          .string()
          .regex(/^[0-9a-fA-F]{24}$/)
          .required()
          .messages({
            'any.required': `"fornecedor id" é um campo obrigatório`,
            'string.empty': `"fornecedor id" não deve ser vazio`,
            'string.pattern.base': `"fornecedor id" fora do formato experado`,
          }),
        autoservice_id: joi
          .string()
          .regex(/^[0-9a-fA-F]{24}$/)
          .required()
          .messages({
            'any.required': `"fornecedor id" é um campo obrigatório`,
            'string.empty': `"fornecedor id" não deve ser vazio`,
            'string.pattern.base': `"fornecedor id" fora do formato experado`,
          }),
      }),
    ),
    asyncMiddleware(autoservicesController.deleteAutoservice),
  );
};
