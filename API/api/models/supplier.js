const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const supplierSchema = {
  name: {
    type: String,
    required: true,
  },
  NIPC: {
    type: String,
    required: true,
  },
  phone:{
    type: String,
    required: true,
  },
  street : {
    type: String,
    required: false,
    },
  zipcode: {
    type: String,
    required: true
  },
  status: {
    // pending, approved, refused
    type: String,
    // required: true,
  },
  image: {
    name: {
      type: String,
      required: false,
    },
    updatedName: {
      type: String,
      required: false,
    },
    type: {
      type: String,
      required: false,
    },
  },
  autoservice_id: [{
    type: Schema.Types.ObjectId,
    ref: 'autoservice_id'
  }],
  like_id: [{
    type: Schema.Types.ObjectId,
    ref: 'like_id'
  }]
};

module.exports = supplierSchema;
