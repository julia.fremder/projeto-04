const mongoose = require('mongoose')
const Schema = mongoose.Schema

module.exports = {
  supplier_id : {
    type: Schema.Types.ObjectId,
    required: true,
    ref: 'supplier_id'
  },
  client_id: {
    type: Schema.Types.ObjectId,
    required: true,
    ref: 'client_id'
  }
}