const mongoose = require('mongoose')
const Schema = mongoose.Schema;

module.exports = {
   name: {
     type: String,
     required: true,
   },
  description: {
    type: String,
    required: false,
  },
  status: {
    type: Boolean,
    // required: true,
  },
  image: {
      name: {
      type: String,
      required: false,
    },
    updatedName: {
      type: String,
      required: false,
    },
    type: {
      type: String,
      required: false,
    },
  },

  autoservice_id: [{
    type: Schema.Types.ObjectId,
    ref: 'autoservice_id'
  }]
  
}