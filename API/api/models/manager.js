const managerSchema = {
  name: {
    type: String,
    required: true,
  },
};

module.exports = managerSchema;
