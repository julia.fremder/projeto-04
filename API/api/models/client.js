const { Schema } = require("mongoose");

const clientSchema = {
  name: {
    type: String,
    required: true,
  },
  NIF: {
    type: String,
    required: true,
  },
  phone:{
    type: String,
    required: true,
  },
  street : {
    type: String,
    required: false,
    },
  zipcode: {
    type: String,
    required: true
  },
  image: {
    name: {
      type: String,
      required: false,
    },
    updatedName: {
      type: String,
      required: false,
    },
    type: {
      type: String,
      required: false,
    },
  },

  likes: [{
    type: Schema.Types.ObjectId,
    ref: 'like_id'
  }]
};

module.exports = clientSchema;
