const mongoose = require('mongoose')
const Schema = mongoose.Schema;

module.exports = {
  name: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: false,
  },
  cost: {
    type: Number,
    required: true,
  },
  image: {
    name: {
      type: String,
      required: false,
    },
    updatedName: {
      type: String,
      required: false,
    },
    type: {
      type: String,
      required: false,
    },
  },

  category: {
    type: Schema.Types.ObjectId,
    ref: 'category'
  },

  supplier: {
    type: Schema.Types.ObjectId,
    ref: 'supplier'
  }
  
};
