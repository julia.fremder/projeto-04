const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const createSchema = (modelOne, model, options = {}) => {
  return new Schema(
    {
      ...modelOne,
      ...model,
    },
    {
      timestamps: true,
      collection: 'UserCollection',
      ...options,
    },
  );
};

// ## USER
const userSchema = require('./user');
const user = mongoose.model(
  'user',
  createSchema(undefined, userSchema, {
    discriminatorKey: 'kind',
  }),
);

const managerSchema = require('./manager');
const manager = user.discriminator(
  'Manager',
  createSchema(userSchema, managerSchema, {}),
);

const clientSchema = require('./client');
const client = user.discriminator(
  'Client',
  createSchema(userSchema, clientSchema, {}),
);

const supplierSchema = require('./supplier');
const supplier = user.discriminator(
  'Supplier',
  createSchema(userSchema, supplierSchema, {}),
);

// ## CATEGORY

const categorySchema = require('./category');
const category = mongoose.model(
  'category',
  createSchema(undefined, categorySchema, {
    collection: 'CategoryCollection',
    toJSON: {
      virtuals: true,
    },
  }),
);

// ## LIKES ## //
const likeSchema = require('./likes');
const like = mongoose.model('likes', createSchema(undefined, likeSchema, {
  collection: 'likesCollection',
  toJSON: {
    virtuals: true,
  }
}));
// ## Auto Services
const autoserviceSchema = require('./autoservice');
const autoservice = mongoose.model(
  'autoservice',
  createSchema(undefined, autoserviceSchema, {
    collection: 'AutoserviceCollection',
    toJSON: {
      virtuals: true,
    },
  }),
);

module.exports = {
  user,
  manager,
  client,
  supplier,
  category,
  autoservice,
  like
};
