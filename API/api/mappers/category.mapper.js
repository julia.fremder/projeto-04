const fileUtils = require('../utils/file.utils');

const toListDTO = (model) => {

const { _id, name, status, description, image } = model;

return {
    id: _id,
     name,
     status,
     description,
     image: fileUtils.addDownloadPath('categories', image.name),
}

}

const toDTO = (model) => {

 const { _id, name, description, status, image, autoservice_id } = model;

  return {
    id: _id,
    name,
    description,
    status,
    image: fileUtils.addDownloadPath('categories', image.name),
    autoservice_id

  }

}

module.exports = {
  toDTO,
  toListDTO,
}