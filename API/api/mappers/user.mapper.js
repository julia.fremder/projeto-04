const fileUtils = require('../utils/file.utils');

const userDTO = (model) => {
  const { _id, email, kind, name, zipcode, street, NIPC, NIF, image, status, autoservices, phone } =
    model;

  return {
    id: _id,
    name,
    email,
    kind,
    NIF: NIPC ? NIPC : NIF || null,
    zipcode: zipcode ? zipcode : null,
    street: street ? street : null,
    phone: phone ? phone : null,
    status: status ? status: null,
    image: image ? fileUtils.addDownloadPath('suppliers', image.name) : null,
    autoservices: autoservices ? autoservices : null,
  };
};

module.exports = {
  userDTO,
};
