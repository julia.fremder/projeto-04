const fileUtils = require('../utils/file.utils');

const toListAutoservicesDTO = (model) => {

const { _id, name, description, image, cost, category, supplier_id } = model;

return {
     id: _id,
     name,
     cost: `€ ${cost.toString()}`,
     category_name: category.name,
     category_id: category._id,
     supplier_id: supplier_id,
     description,
     image: fileUtils.addDownloadPath('autoservices', image.name),
}

}

// const toDTO = (model) => {

//  const { _id, name, description, status, image } = model;

//   return {
//     id: _id,
//     name,
//     description,
//     status,
//     image: fileUtils.addDownloadPath('categories', image.name),
//   }

// }

module.exports = {
  toListAutoservicesDTO,
}