const autoservicesService = require('../../services/autoservices.service');

addAutoservices = async (req, res, next) => {
  const { body, params } = req;

  const result = await autoservicesService.AddNewAutoservice({
    ...params,
    ...body,
  });

  // const statusCode = result.success ? 200 : 400;
  // const dataReturned = result.success
  //   ? { data: result.data }
  //   : { message: result.message, data: result.details };

  return res.status(200).send({data: result.data});
};

getListAutoservices = async (req, res, next) => {
  const { query } = req;

  const result = await autoservicesService.filterAutoservice(query);

  // const statusCode = result.success ? 200 : 400;
  // const dataReturned = result.success
  //   ? { data: result.data }
  //   : { message: result.message, data: result.details };

  return res.status(200).send({data: result.data});
};

const deleteAutoservice = async (req, res, next) => {
  const { supplier_id, autoservice_id } = req.params;

  const { user } = req;
  const { id } = user;
  const result = await autoservicesService.deleteAutoservice(
    supplier_id,
    autoservice_id,
    id,
  );

  // const statusCode = result.success ? 200 : 400;
  // const dataReturned = result.success
  //   ? { data: result.data }
  //   : { message: result.message, data: result.details };

  return res.status(200).send({data: result.data});
};

module.exports = {
  addAutoservices,
  getListAutoservices,
  deleteAutoservice,
};
