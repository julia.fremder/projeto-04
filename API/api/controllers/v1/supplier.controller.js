const supplierService = require('../../services/supplier.service');

const newSupplier = async (req, res, next) => {
  const { body } = req;
  const result = await supplierService.addSupplier(body);

  // const returnedCode = result.success ? 200 : 400;
  // const returnedData = result.success
  //   ? { data: result.data }
  //   : { message: result.message, data: result.details };

  return res.status(200).send({data: result});
};

const findSupplier = async (req, res, next) => {
  const { params } = req;

  const supplier = await supplierService.findItemById(params.supplier_id);

  // if (!supplier)
  //   return res.status(404).send({
  //     details: ['Não há fornecedor com este id cadastrado na base de dados.'],
  //   });

  return res.status(200).send(supplier);
};

const updateSupplier = async (req, res, next) => {
  const { params, body } = req;

  const result = await supplierService.updateSupplier(params.supplier_id, body);

  // const statusCode = result.success ? 200 : 400;
  // const dataReturned = result.success
  //   ? { data: result.data }
  //   : { message: result.message, data: result.details };

  return res.status(200).send({data: result});
};

const approveSupplier = async (req, res, next) => {
  const { params } = req;
  const result = await supplierService.updateStatusSupplier(
    params.supplier_id,
    'Approved',
  );
  // const statusCode = result.success ? 200 : 400;
  // const dataReturned = result.success
  //   ? { data: result.data }
  //   : { message: result.message, data: result.details };

  return res.status(200).send({data: result});
};
const refuseSupplier = async (req, res, next) => {
  const { params } = req;
  const result = await supplierService.updateStatusSupplier(
    params.supplier_id,
    'Refused',
  );
  // const statusCode = result.success ? 200 : 400;
  // const dataReturned = result.success
  //   ? { data: result.data }
  //   : { message: result.message, data: result.details };

  return res.status(200).send({data: result});
};
const listSuppliers = async (req, res, next) => {
  const result = await supplierService.findAll();

  return res.status(200).send({ data: result});
};
module.exports = {
  newSupplier,
  updateSupplier,
  approveSupplier,
  refuseSupplier,
  listSuppliers,
  findSupplier,
};
