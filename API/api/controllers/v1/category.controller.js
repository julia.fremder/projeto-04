const category = require('../../models/category');
const categoryService = require('../../services/category.service');

const addCategory = async (req, res, next) => {
  const { body } = req;

  const result = await categoryService.addCategory(body);

  // const statusCode = result.success ? 200 : 400;
  // const dataReturned = result.success
  //   ? { data: result.data }
  //   : { message: result.message, data: result.details };

  return res.status(200).send({data: result.data});
};

const updateCategory = async (req, res, next) => {
  const { params, body } = req;

  const resultService = await categoryService.updateItem(
    params.category_id,
    body,
  );

  // const statusCode = resultService.success ? 200 : 400;
  // const dataReturned = resultService.success
  //   ? { data: resultService.data }
  //   : { data: resultService.details };

  return res.status(200).send({data: result.data});
};

const findCategory = async (req, res, next) => {
  const { params } = req;

  const category = await categoryService.findItemById(params.category_id);

  // if (!category)
  //   return res.status(404).send({
  //     details: ['Não há categoria com este id cadastrada na base de dados.'],
  //   });

  return res.status(200).send({data: result.data});
};

const listCategories = async (req, res, next) => {
  const result = await categoryService.findAll();

  return res.status(200).send({ data: result });
};

const deleteCategory = async (req, res, next) => {
  const { params } = req;
  const resultService = await categoryService.deleteItem(params.category_id);

  const statusCode = resultService.success ? 200 : 400;
  const dataReturned = resultService.success
    ? { message: resultService.message }
    : { data: resultService.details };
  return res.status(statusCode).send(dataReturned);
};

module.exports = {
  addCategory,
  updateCategory,
  findCategory,
  listCategories,
  deleteCategory,
};
