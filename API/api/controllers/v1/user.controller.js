const userService = require('../../services/user.service');

const auth = async (req, res) => {
  const { email, password } = req.body;

  const result = await userService.authenticate(email, password);

  // const statusReturned = result.success ? 200 : 401;
  // const dataReturned = result.success
  //   ? { message: result.message, data: result.data }
  //   : { message: result.message, data: result.details };
  return res.status(200).send({ message: result.message, data: result.data });
  // return res.status(statusReturned).send(dataReturned);
};

const deleteUser = async (req, res, next) => {
  const { params } = req;
  const result = await userService.deleteUser(params.id);

  const statusCode = result.success ? 200 : 400;
  const dataReturned = result.success
    ? { message: result.message }
    : { data: result.details };
  return res.status(statusCode).send(dataReturned);
};

module.exports = {
  auth,
  deleteUser,
};
