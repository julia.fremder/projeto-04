const clientService = require('../../services/client.service');

const addClient = async (req, res, next) => {
  const { body } = req;

  const result = await clientService.addClient(body);

  // const returnedCode = result.success ? 200 : 400;
  // const returnedData = result.success
  //   ? { data: result.data }
  //   : { message: result.message, data: result.details };

  return res.status(200).send({data: result.data});
};

const seeLikes = async (req, res, next) => {
  return res.status(200).send({
    data: [{ mock: 'mockado' }],
  });
};

const findClientById = async (req, res, next) => {
  return res.status(200).send({ mock: 'mockado' });
};

module.exports = {
  addClient,
  seeLikes,
  findClientById,
  // realizaCurtida,
};
