const md5 = require('md5');
const jwt = require('jsonwebtoken');
const md5HashSecret = process.env.MD5_SECRET;
const jwtHashSecret = process.env.JWT_SECRET;
const jwtTimeLimit = process.env.JWT_VALID_TIME;

const addHash = (password) => md5(password + md5HashSecret);

const addToken = (model) => {
  const signature = jwt.sign({...model}, `${jwtHashSecret}`, {
    expiresIn: `${jwtTimeLimit}d`,
  });
  return signature
};

const verifyToken = (token) => {
  try {
    jwt.verify(token, `${jwtHashSecret}`);
    return true;
  } catch (error) {
    // console.log(error, 'error do verify')
    return false;
  }
}

const decodeToken = (token) => {
  return jwt.decode(token)
};

module.exports = {
  addHash,
  addToken,
  verifyToken,
  decodeToken,
};
