const sgMail = require('@sendgrid/mail')
sgMail.setApiKey(process.env.SG_API_KEY)

const send = async ({recipient, from, subject, text}) => {

try {
  const email = {
    to: recipient,
    from,
    subject,
    text
  }
  
  const dataReturned = await sgMail.send(email)
 
} catch (error) {
  //console.log(error.response.body, 'error.response.body')
}
}

module.exports = {
  send,
}