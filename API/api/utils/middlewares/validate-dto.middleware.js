const Joi = require('joi');
const ErrorBusinessRule = require('../errors/err-business-rule')

const validateDTO = (type, parameter, options = {}) => {
  return async (req, res, next) => {
    const schema =  Joi.object().keys(parameter);
    const result =  schema.validate(req[type], {
      allowUnknown: false,
      ...options,
    });
    if (result.error) {
      const messages = result.error.details.reduce((acc, item) => {
        return [...acc, item.message];
      }, []);
      throw new ErrorBusinessRule(messages)
      // return res.status(400).send({
      //   success: false,
      //   details: messages,
      // });
    }
    return next();
  };
};

module.exports = validateDTO;
