const { IncomingForm } = require('formidable');
const formidable = require('formidable');
const ErrorBusinessRule = require('../errors/err-business-rule')

const fileUtils = require('../file.utils');

const postIsValid = (files) => {
  if (!files.image || files.image.name === '') {
    return false;
  }
  return true;
};

const putIsValid = (files) => {
  if (!files.image || files.image.name === '') {
    return false;
  }
  return true;
};

const fileUpload = (destiny, isUpdate = false) => {
  return async (req, res, next) => {
    const form = formidable.IncomingForm();

    form.uploadDir = fileUtils.addPath('temp');

    let formFields = await new Promise(function (resolve, reject) {
      form.parse(req, (err, fields, files) => {
        if (err) {
          return reject(err);
        }

        resolve({
          ...fields,
          files,
        });
      });
    }); 
    const { files, ...fields } = formFields;

    req.body = {
      ...fields,
    };

    if (req.method === 'POST') {
      if (!postIsValid(files))
        throw new ErrorBusinessRule('É obrigatório cadastrar imagem.');
    }

    if (files.image && files.image.name !== '') {
      const updatedName = fileUtils.updateName(files.image.type);
      const updatedPath = fileUtils.addPath(destiny, updatedName);

      req.body.image = {
        type: files.image.type,
        name: files.image.name,
        path: files.image.path,
        updatedName,
        updatedPath,
      };
    }

    next();
  };
};

module.exports = fileUpload;
