const cryptographyUtils = require('../cryptography.utils')
const userService = require('../../services/user.service');

const authorize = (rota = '*') => {

  return async (req, res, next) => {

    const { token } = req.headers;

    if (!token) {
      return res.status(403).send({
        message: "usuário não autorizado."
      })
    }
const resultVerify = cryptographyUtils.verifyToken(token)

    if (!resultVerify) {
      return res
        .status(401)
        .send({ message: "usuário não autenticado." });
    }

    const { id, email, kind } = cryptographyUtils.decodeToken(token);
 
    if (!(await userService.checkEmail(email))) {
      return res.status(403).send({
        message: "usuário não autorizado."
      })
    }

    //TODO: verificar se o usuario informado pussui o privilégio necessario   para executar a rota.
    if (rota != '*') {
      if (!userService.checkPermitionsByProfile(kind, rota))
        return res.status(403).send({
          message: "usuário não autorizado."
        })

    }

    //TODO: incluir usuario na request
    req.user = {
      id,
      email,
      kind,
    }


    return next();

  }

}

module.exports = authorize;

