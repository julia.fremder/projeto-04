const ErrorGeneric = require('../errors/err-generic');

const asyncMiddleware = (fn, options) => (req, res, next) => {

  fn(req, res, next)
    .catch(err => {
      if (err instanceof ErrorGeneric) {
        return res.status(err.statusCode).send({
          message: [err.message],
        });
      } else {
        return res.status(500).send({message: [ "Ocoreu um erro interno, solicitar adminsitrador da solução"]
          
        });

      }
    });

}

module.exports = asyncMiddleware;