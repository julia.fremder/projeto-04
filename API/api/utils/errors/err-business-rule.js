const status = require('http-status');
const defaultMessage = 'Aconteceu um erro de negócio';

const ErrorGeneric = require('./err-generic');

module.exports = class ErrorBusinessRule extends ErrorGeneric {

  constructor(message) {
    super(message);
    Error.captureStackTrace(this, ErrorBusinessRule);
    this.statusCode = status.BAD_REQUEST;
    this.message = message || defaultMessage;
  }

}