const status = require('http-status');
const defaultMessage = 'O Usuário não tem autorização para acessar.';
const ErrorGeneric = require('./err-generic');

module.exports = class ErrorUserNotAuthorized extends ErrorGeneric {

  constructor(message) {
    super(message);
    Error.captureStackTrace(this, ErrorUserNotAuthorized);
    this.statusCode = status.FORBIDDEN;
    this.message = message || defaultMessage;
  }

}