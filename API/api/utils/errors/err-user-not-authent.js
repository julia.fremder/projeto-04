const status = require('http-status');
const defaultMessage = 'O usuário não está autenticado.';
const ErrorGeneric = require('./err-generic');

module.exports = class ErrorUserNotAuthenticated extends ErrorGeneric {

  constructor(message) {
    super(message);
    Error.captureStackTrace(this, ErrorUserNotAuthenticated);
    this.statusCode = status.UNAUTHORIZED;
    this.message = message || defaultMessage;
  }

}