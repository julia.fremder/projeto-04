const path = require('path');
const fs = require('fs');

const uuid = require('uuid').v4;

const basePath = process.env.FILE_BASE_PATH;

const addPath = (destiny, fileName = "") => {
  return path.join(`/${basePath}/${destiny}/${fileName}`);
}

const addDownloadPath = (origin, fileName) => {
   return path.join(`/static/${origin}/${fileName}`);
}

const updateName = (type) => {
   const typeUpdated = type.split('/')[1];
   return `${uuid()}.${typeUpdated}`;
}

const move = (temp, definitive) => {
  return fs.renameSync(temp, definitive);
}

const remove = (origin, fileName) => {
  const filePath = addPath(origin, fileName);

   if (fs.existsSync(filePath))
     fs.unlinkSync(filePath);

   return;
}


module.exports = {
     addPath,
     addDownloadPath,
     updateName,
     move,
     remove,
}