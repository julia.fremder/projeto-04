import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { signInAction, signOutAction } from './store/auth/auth.action'
import {
  addNewSupplier,
  getSuppliersList,
  getDataSupplierById,
  updateStatusSupplier
} from './store/supplier/supplier.actions'
import {
  getCategoriesList,
  getDataCategoryById,
  updateCategoryById,
  addNewCategory,
  deleteCategoryById
} from './store/category/category.action'
import {
  addNewAutoservice,
  deleteAutoserviceById,
  getAutoservicesList
} from './store/autoservice/autoservice.actions'
import { addNewClient, getClientById } from './store/costumer/client.actions'
//------------------COMPONENTS-------------------------------//
import ModalSigInHome from './components/ModalSignInHome'
import SignUpSupplier from './components/SignUpSupplier'
import SignUpClient from './components/SignUpClient'
import ManagerDefault from './components/ManagerDefault'
import Categories from './components/Categories'
import CategoryDetails from './components/CategoryDetails'
import ModalAddCategory from './components/ModalAddCategory'
import ModalEditCategory from './components/ModalEditCategory'
import ModalAddAutoservice from './components/ModalAddAutoservice'
import Suppliers from './components/Suppliers'
import DetailsSupplier from './components/DetailsSupplier'
import DeleteCategory from './components/DeleteCategory'
import SupplierMessage from './components/SupplierMessage'
import DashSupplier from './components/DashSupplier'
import Client from './components/ChildrenDashClient'

export const GlobalContext = React.createContext()

export const GlobalHome = ({ children }) => {
  const dispatch = useDispatch()
  //---------LOCAL STATES---------------//
  const [modal, setModal] = React.useState({ status: false, section: 'Home' })
  const [form, setForm] = React.useState({})
  const [msgForm, setMsgForm] = React.useState({})
  const [preview, setPreview] = React.useState('')
  const [dropdownOpen, setDropdownOpen] = React.useState(false)
  //-----------STORE-------------------//
  const user = useSelector((state) => state.auth.user)
  const signErrors = useSelector((state) => state.auth.errors)
  const categories = useSelector((state) => state.category.categories)
  const details = useSelector((state) => state.category.details)
  const suppliers = useSelector((state) => state.supplier.suppliers)
  const detailsSupplier = useSelector((state) => state.supplier.details)
  const autoservices = useSelector((state) => state.autoservice.autoservices)

  //-------------setForm------------//
  function handleChange(e) {
    const { value, name } = e.target
    setForm({
      ...form,
      [name]: value
    })
  }
  function validateEmail(email) {
    let re = /\S+@\S+\.\S+/
    return re.test(email)
  }
  function handleMessage(e) {
    const { value, name } = e.target
    let message = ''
    switch (name) {
      case 'NIPC':
        if (value.length !== 9) {
          message = 'Numero fiscal da empresa deve ter 9 dígitos xxxxxxxxx.'
        }
        break
      case 'email':
        if (validateEmail(value) === false) {
          message = 'E-mail em formato inválido.'
        }
        break
      case 'NIF':
        if (value.length !== 9) {
          message = 'Número fiscal deve ter 9 dígitos xxxxxxxxx.'
        }
        break
    }
    setMsgForm({
      [name]: message
    })
  }
  //--Sends request to SIGNIN--//
  const handleSubmit = (section, data, id) => {
    if (section === 'SignIn') {
      dispatch(signInAction(data))
        .then(() => {
          setForm({})
        })
        .then(() =>
          user ? setModal({ section: user.id }) : setModal({ section: 'Home' })
        )
        .catch((err) => toggle('Home'))
    }
    if (section === 'SignUpSupplier') {
      dispatch(addNewSupplier(data)).then(() => toggle('Home')).catch(() => toggle('SignUpSupplier'))
    }

    if (section === 'SignUpClient') {
      dispatch(addNewClient(data))
        .then(() => {
          toggle('SignIn')
          setForm({ email: data.email, password: data.password })
        })
        .catch(() => toggle('Home'))
    }
    if (section === 'Category') {
      id
        ? dispatch(updateCategoryById(id, data))
        : dispatch(addNewCategory({ status: true, ...data }))
            .then(() => {
              toggle('Categories')
              dispatch(getCategoriesList())
            }).catch((err) => toggle('Category'))
    }
    if (section === 'DetailSupplier') {
      toggle('DetailSupplier')
      dispatch(getDataSupplierById(id))
        .then(() => dispatch(getAutoservicesList({ supplier: id })))
        .catch((err) => toggle('DetailSupplier'))
    }
    if (section === 'Update') {
      dispatch(updateStatusSupplier(data, id))
    }
    if (section === 'AddAutoservice') {
      dispatch(
        addNewAutoservice(
          {
            status: true,
            ...data
          },
          id
        )
      ).then(() => {
        toggle('Supplier')
        dispatch(getAutoservicesList({ supplier: id })).catch((err) =>
          toggle('AddAutoservice')
        )
      })
    }
  }

  const sortedSuppliers = suppliers.sort(function (obj1, obj2) {
    var nameA = obj1.name.toUpperCase() // ignore upper and lowercase
    var nameB = obj2.name.toUpperCase() // ignore upper and lowercase
    if (nameA < nameB) {
      return -1
    }
    if (nameA > nameB) {
      return 1
    }
    return 0
  })
  //--create url to preview the image is being added to crud--//
  const previewImg = (props) => {
    const image = props.target.files[0]
    const url = URL.createObjectURL(image)
    setPreview(url)
    setForm({
      ...form,
      image
    })
  }

  //--Cleans form and preview of image when modal is closed--//
  React.useEffect(() => {
    dispatch(getSuppliersList())
    if (!user) {
      setForm({})
      setModal({ status: false, section: 'Home' })
    }

    if (user && user.kind === 'Manager') {
      toggle('Manager')
      dispatch(getCategoriesList()).then(() =>
        dispatch(getAutoservicesList())
          .then(() => toggle('Manager'))
          .catch((err) => toggle('Manager'))
      )
    }
  }, [user])
  React.useEffect(() => {
    if (user && user.kind === 'Supplier') {
      dispatch(getDataSupplierById(user.id))
        .then(() => toggle('Supplier'))
        .catch((err) => toggle('Supplier'))
      dispatch(getAutoservicesList({ supplier: user.id }))
      dispatch(getCategoriesList())
    }
  }, [user])
  React.useEffect(() => {
    const cleanForm = (modal) => {
      if (!modal.status) {
        delete form.imagem
        setForm({})
        setPreview('')
        setMsgForm({})
      }
    }
    cleanForm(modal)
  }, [modal])

  //-----SIGN OUT METHOD-----//
  //--Sends logout request in the exit button--//
  function handleClick() {
    dispatch(signOutAction())
    setForm({})
    setMsgForm({})
    toggle('Home')
  }
  function fillForm() {
    if (preview === '') {
      setPreview(process.env.REACT_APP_API + details?.image)
      setForm({
        id: details.id,
        name: details.name,
        description: details.description,
        status: true,
        ...form
      })
    }
    if (preview !== '') {
      setPreview('')
      setForm({ status: true })
    }
  }
  function removeImage() {
    delete form.imagem
    setForm(form)
    setPreview('')
  }

  //---------RE-RENDER WHEN SUPPLIER STATUS CHANGE ----//
  function changeStatus(id, status) {
    setForm({ ...form, status: status })
    dispatch(updateStatusSupplier(id, status))
      .then(() => dispatch(getDataSupplierById(id)))
      .catch((err) => dispatch(getDataSupplierById(id)))
  }

  function detailComponent(section) {
    switch (section) {
      case 'Edit':
        return <ModalEditCategory />
      case 'Detail':
        return <CategoryDetails />
      case 'DetailSupplier':
        return <DetailsSupplier />
      case 'Suppliers':
        return <Suppliers />
      case 'Delete':
        return <DeleteCategory />
      case 'AddCategory':
        return <ModalAddCategory />
      case 'AddAutoservice':
        return <ModalAddAutoservice />
      case 'Categories':
        return <Categories />
      case 'Update':
        return <UpdateStatusSupplier />
      case 'SignIn':
        return <ModalSigInHome />
      case 'Supplier':
        return <DashSupplier />
      case 'Client':
        return <Client />
      case 'SupplierMessage':
        return <SupplierMessage />
      case 'Manager':
        return <ManagerDefault />
      case 'SignUpSupplier':
        return <SignUpSupplier />
      case 'SignUpClient':
        return <SignUpClient />
      default:
        return ''
    }
  }

  //--open and close modalDash--//
  function toggle(section) {
    if (section === 'Manager' || section === 'Supplier' || section === 'Categories') {
      setModal({ section: section, status: false })
      setPreview('')
      setForm({})
      setMsgForm({})
    }
    if (section !== 'Manager' && section !== 'Home') {
      setModal({ section: section, status: true })
    }
    if (section === 'Home') {
      setModal({ section: '', status: false })
      setForm({})
      setMsgForm({})
    }
  }

  function toggleUserMenu() {
    setDropdownOpen((prevState) => !prevState)
  }

  //------Get Categories Details----//
  function clickCategory(section, id) {
    if (section === 'Detail') {
      toggle('Detail')
    }
    if (section === 'Edit') {
      toggle('Edit')
    }
    if (section === 'Delete') {
      toggle('Delete')
    }
    dispatch(getDataCategoryById(id))
  }

  function deleteCategory(id) {
    dispatch(deleteCategoryById(id))
    setTimeout(() => {
      toggle('Categories')
      dispatch(getCategoriesList())
    }, 500)
  }
  function deleteAutoservice(supplier_id, autoservice_id) {
    dispatch(deleteAutoserviceById(supplier_id, autoservice_id))
      .then(() => dispatch(getAutoservicesList({ supplier: supplier_id })))
      .catch((err) => console.log(err))
  }
  return (
    <GlobalContext.Provider
      value={{
        dispatch,
        modal,
        setModal,
        form,
        setForm,
        previewImg,
        handleSubmit,
        suppliers,
        handleChange,
        preview,
        setPreview,
        user,
        categories,
        details,
        suppliers,
        detailsSupplier,
        autoservices,
        dropdownOpen,
        setDropdownOpen,
        toggleUserMenu,
        handleClick,
        dispatch,
        detailComponent,
        handleSubmit,
        deleteCategory,
        clickCategory,
        toggle,
        removeImage,
        fillForm,
        changeStatus,
        handleMessage,
        msgForm,
        deleteAutoservice
      }}
    >
      {children}
    </GlobalContext.Provider>
  )
}
