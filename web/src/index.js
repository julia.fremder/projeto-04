/* eslint-disable jsx-quotes */
import React from 'react'
import ReactDOM from 'react-dom'
import { Helmet } from 'react-helmet'
import { Provider } from 'react-redux'
import store from './store'
import ReduxToastr from 'react-redux-toastr'
import GlobalStyled from './styles/globalStyled'
import { ThemeProvider } from 'styled-components'
import theme from './styles/theme'
import 'bootstrap/dist/css/bootstrap.min.css'
import 'react-redux-toastr/lib/css/react-redux-toastr.min.css'
import Routes from './Routes'
import { GlobalHome } from './GlobalContext'

const googleFont =
  'https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap'

ReactDOM.render(
  <Provider store={store}>
    <GlobalHome>
        <ReduxToastr />
        <Helmet>
          <link rel="stylesheet" href={googleFont} />
        </Helmet>
        <ThemeProvider theme={theme}>
          <GlobalStyled />
          <Routes />
        </ThemeProvider>
    </GlobalHome>
  </Provider>,
  document.getElementById('root')
)
