import { createGlobalStyle } from 'styled-components'
import wallpaper from '../assets/image/wallpaper2.jpg'

const GlobalStyle = createGlobalStyle`

    * {
        margin: 0;
        padding: 0;
        outline: 0;
        font-family: 'Roboto';
        list-style: none;
    }

    #route-main{
        display:flex;
        flex-direction:column;
        min-height: 100vh;
        
            .btn-amarelo{
                border-radius: 1.5rem;
                background-color: #ecc745;
                color: black;
                margin: 0 0.5rem;
                font-size: 0.9rem;
                border: 0 solid transparent;
                -webkit-box-shadow: 2px 2px 4px 1px rgba(0, 0, 0, 0.31);
                box-shadow: 2px 2px 4px 1px rgba(0, 0, 0, 0.31);
                cursor: pointer;
                &:hover{
                    background-color: #ccc;
                }
            }
            .align-center{
                display: flex;
                flex-direction: column;
                align-items: center;
                justify-content: center;
            }
            .d-flex{
                display: flex;
                justify-content: center;
            }
        
            main{
                display: flex;
                flex-direction: column;
                align-items: center;
                flex: 1;
                background-image: url(${wallpaper});
                background-repeat: repeat;
                
                .collapsed-btn {
                    margin-top: 12vh !important;
                    margin-bottom: 0.5rem;
                    background-color: #ecc745;
                    color: black;
                    font-size: 1.2rem;
                    height: 4rem;
                    border: 0px solid transparent;
                    border-radius: 0.1rem !important;
                    width: 90vw;
                    -webkit-box-shadow: 2px 2px 4px 1px rgba(0, 0, 0, 0.31);
                    box-shadow: 2px 2px 4px 1px rgba(0, 0, 0, 0.31);
                    svg {
                        font-size: 1.5rem;
                    }
                    @media (min-width: 768px) {
                        width: 70vw;
                    }
                }
                .collapsed-btn:hover {
                    background-color: #eee;
                    color: black;
                    font-weight: bold;
                    font-size: 1.3rem;
                }
            }
        }
`

export default GlobalStyle
