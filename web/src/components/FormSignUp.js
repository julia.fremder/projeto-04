import React from 'react'
import { Form, FormGroup, Input, Label, FormFeedback } from 'reactstrap'
import styled from 'styled-components'
import { GlobalContext } from '../GlobalContext'

const FormSignUp = () => {
  const global = React.useContext(GlobalContext)
  const { handleChange, handleMessage, msgForm, form, preview, previewImg } = global
  
 
  return (
    <SForm>
      <FormGroup>
        <Label htmlFor="companyName">Nome</Label>
        <Input
          type="text"
          name="name"
          placeholder="Nome da Empresa."
          onChange={handleChange}
          value={form.name || ''}
        />
      </FormGroup>
      <FormGroup>
        <Label htmlFor="email">Email</Label>
        <Input
          type="text"
          name="email"
          placeholder="E-mail de contato"
          onChange={handleChange}
          value={form.email || ''}
        />
      </FormGroup>
      <FormGroup>
        <Label htmlFor="phone">Contacto</Label>
        <Input
          type="text"
          name="phone"
          placeholder="Contacto telefónico"
          onChange={handleChange}
          value={form.phone || ''}
        />
      </FormGroup>
      <FormGroup>
        <Label htmlFor="zipcode">Código Postal</Label>
        <Input
          type="text"
          name="zipcode"
          placeholder="Código postal"
          onChange={handleChange}
          value={form.zipcode || ''}
        />
      </FormGroup>
      <FormGroup>
        <Label htmlFor="street">Morada</Label>
        <Input
          type="text"
          name="street"
          placeholder="Rua, praça, praceta ..."
          onChange={handleChange}
          value={form.street || ''}
        />
      </FormGroup>
      <FormGroup>
        <Label htmlFor="NIPC">NIPC</Label>
        <Input
          type="text"
          name="NIPC"
          maxLength="9"
          placeholder="Número fiscal."
          invalid={msgForm.NIPC ? true : false}
          value={form.NIPC || ''}
          onChange={(e) => {
            handleMessage(e)
            handleChange(e)
          }}
        />
        <FormFeedback>{msgForm.NIPC || ''}</FormFeedback>
      </FormGroup>
      <FormGroup>
        <Label htmlFor="password">Palavra-passe</Label>
        <Input
          type="password"
          name="password"
          placeholder="Escolha uma palavra-passe."
          onChange={handleChange}
          value={form.password || ''}
        />
      </FormGroup>
      <FormGroup>
        <Label htmlFor="image">Logomarca</Label>
        <div className="modal-image">
          {preview ? <img src={preview} /> : <p>Imagem</p>}
        </div>
        <Input
          type="file"
          name="image"
          accept="image/*"
          onChange={previewImg}
        />
      </FormGroup>
    </SForm>
  )
}

export default FormSignUp

const SForm = styled(Form)`
  display: flex;
  flex-direction: column;
  flex: 1;

  .form-group {
    margin: 0.7rem 0;
    label {
      font-size: 0.8rem;
      color: #696969;
    }
    input[type='text'] {
      color: #696969;
      font-size: 0.95rem;
    }
    input[type='file'] {
      color: #696969;
    }
  }

  .modal-image {
    width: 200px;
    height: 200px;
    object-fit: cover;
    overflow: hidden;
    border: 0.3px solid #eee;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    margin-bottom: 0.5rem;
    p {
      color: #696969;
    }
  }
`
