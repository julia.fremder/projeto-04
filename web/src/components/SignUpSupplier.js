import React from 'react'
import { Modal, ModalBody, ModalFooter, ModalHeader, Button } from 'reactstrap'
import { IoPersonOutline } from 'react-icons/io5'
import { GiAutoRepair } from 'react-icons/gi'
import Logo from '../assets/image/oficinadvisor2.png'
import FormSignInHome from './FormSignInHome'
import styled from 'styled-components'
import { GlobalContext } from '../GlobalContext'
import FormSignUp from './FormSignUp'

const SignUpSupplier = () => {
  const global = React.useContext(GlobalContext)
  const { toggle, modal, handleSubmit, form } = global

  return (
    <>
      <SModal
        toggle={() => toggle('Home')}
        isOpen={modal.section === 'SignUpSupplier' && modal.status}
      >
        <ModalHeader toggle={() => toggle('Home')}>
          <img src={Logo} />
          <p>Cadastre-se para ser Oficina no Oficinadvisor.</p>
        </ModalHeader>
        <ModalBody>
          <FormSignUp />
        </ModalBody>
        <ModalFooter>
          <div className="left">
            Já se cadastrou?{' '}
            <Button size="sm" color="link" onClick={() => toggle('SignIn')}>
              Faça Login
            </Button>
          </div>
          <div>
            <Button size="sm" onClick={() => handleSubmit('SignUpSupplier', form)}>
              Enviar
            </Button>
          </div>
        </ModalFooter>
      </SModal>
    </>
  )
}

export default SignUpSupplier

const SModal = styled(Modal)`
  max-width: 50vw;
  .modal-header {
    .close {
      border: none;
      background-color: transparent;
      font-size: 1.25rem;
    }
  }
  .modal-body {
    padding: 1rem;
  }
  .modal-footer {
    display: flex;
    align-items: center;
    justify-content: space-between;
    .left {
      font-size: 0.8rem;
      display: flex;
      align-items: center;
      .btn {
        display: flex;
        align-items: center;
        justify-content: center;
        margin-left: 1rem;
        background-color: transparent;
        border: transparent;
        color: #696969;
        font-size: 0.7rem;
        -webkit-box-shadow: 2px 2px 4px 1px rgba(0, 0, 0, 0.31);
        box-shadow: 2px 2px 4px 1px rgba(0, 0, 0, 0.31);
        cursor: pointer;
        &:hover {
          background-color: ${(props) => props.theme.colors.secondary};
        }
        svg {
          font-size: 1rem;
        }
      }
    }
    .btn {
      font-size: 0.8rem;
      background-color: ${(props) => props.theme.colors.secondary};
      color: ${(props) => props.theme.colors.primary};
      border: transparent;
      -webkit-box-shadow: 2px 2px 4px 1px rgba(0, 0, 0, 0.31);
      box-shadow: 2px 2px 4px 1px rgba(0, 0, 0, 0.31);
      cursor: pointer;
    }
  }
  @media (max-width: 768px) {
    max-width: 100vw;
  }
`
