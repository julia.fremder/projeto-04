import React from 'react'
import { Form, FormGroup, Input, Label } from 'reactstrap'
import styled from 'styled-components'
import { GlobalContext } from '../GlobalContext'

const FormSignInHome = () => {
  const global = React.useContext(GlobalContext)
  const { form, handleChange } = global
  return (
    <SForm>
      <FormGroup>
        <Label htmlFor="email">E-mail</Label>
        <Input
          type="email"
          value={form?.email || ''}
          name="email"
          onChange={handleChange}
        />
      </FormGroup>
      <FormGroup>
        <Label htmlFor="password">Palavra-passe</Label>
        <Input
          type="password"
          value={form?.password || ''}
          name="password"
          onChange={handleChange}
        />
      </FormGroup>
    </SForm>
  )
}

export default FormSignInHome

const SForm = styled(Form)`
  display: flex;
  flex-direction: column;
  flex: 1;

  .form-group {
    margin: 0.7rem 0;
    label {
      font-size: 0.8rem;
      color: #696969;
    }
    input[type='text'] {
      color: #696969;
      font-size: 0.95rem;
    }
    input[type='file'] {
      color: #696969;
    }
  }
`
