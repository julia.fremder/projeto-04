import React from 'react'
import { Modal, ModalBody, ModalFooter, ModalHeader, Button } from 'reactstrap'
import FormCategory from './FormCategory'
import styled from 'styled-components'
import { GlobalContext } from '../GlobalContext'

const ModalAddCategory = () => {
  const global = React.useContext(GlobalContext)
  const { modal, toggle, handleSubmit, removeImg, form } = global
  return (
    <SModal
      isOpen={
        modal && modal.section === 'AddCategory'
          ? modal.status
          : false
      }
    >
      <ModalHeader toggle={() => toggle('Categories')}>
        Nova categoria:
      </ModalHeader>
      <ModalBody>
        <FormCategory />
      </ModalBody>
      <ModalFooter>
        <Button
          type="submit"
          onClick={() => handleSubmit('Category', form, null)}
        >
          adicionar
        </Button>
        <Button onClick={() => removeImg()}>remover imagem</Button>
      </ModalFooter>
    </SModal>
  )
}

export default ModalAddCategory

const SModal = styled(Modal)`
  .modal-header {
    .close {
      border: none;
      background-color: transparent;
      font-size: 1.25rem;
    }
  }

  .modal-body {
    padding: 1rem;
  }
`
