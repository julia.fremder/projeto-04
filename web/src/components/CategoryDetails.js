import React from 'react'
import { RiArrowDropUpFill } from 'react-icons/ri'
import { Button } from 'reactstrap'
import styled from 'styled-components'
import { GlobalContext } from '../GlobalContext'

const CategoryDetails = () => {
  const global = React.useContext(GlobalContext)
  const { details, toggle } = global
  return (
    <>
      <Button className="collapsed-btn" onClick={() => toggle('Manager')}>
        Ocultar Categorias
        <RiArrowDropUpFill />
      </Button>
      <Detail>
        <div className="detail-info">
          <div className="detail-image">
            <img src={process.env.REACT_APP_API + details.image}></img>
          </div>
          <div>
            <p>
              <strong>Categoria: </strong>
              {details.name}
            </p>
            <p>
              <strong>Descrição: </strong>
              {details.description}
            </p>
          </div>
        </div>
        <div className="detail-footer">
          <Button onClick={() => toggle('Suppliers-detail')}>
            Fornecedores
          </Button>
          <Button onClick={() => toggle('Servicos-detail')}>Serviços</Button>
          <Button onClick={() => toggle('Categories')}>Voltar</Button>
        </div>
      </Detail>
    </>
  )
}

export default CategoryDetails

const Detail = styled.section`
  display: flex;
  flex-direction: column;
  background-color: #fafafa;
  max-width: 60%;
  padding-top: 20px;
  padding-bottom: 20px;
  font-size: 0.9rem;
  margin: 0rem auto 0.5rem auto;

  @media (max-width: 1024px) {
    max-width: 100%;
    font-size: 1.5vw;
  }

  .detail-info {
    display: flex;
  }
  .detail-image {
    width: 10vw;
    height: 100px;
    overflow: hidden;
    display: flex;
    /* align-items: center; */
    justify-items: center;
    img {
      height: 60px;
      width: auto;
      margin-right: auto;
      margin-left: auto;
    }
  }
  .detail-footer {
    display: flex;
    justify-content: flex-end;
  }
`
