import React from 'react'
import UserDropdownMenu from '../components/UserDropdownMenu'
import styled from 'styled-components'

import { Navbar, Container } from 'reactstrap'
import { GlobalContext } from '../GlobalContext'

const HeaderDash = () => {
  const global = React.useContext(GlobalContext)
  const { user } = global
  return (
    <Header>
      <Container>
        {' '}
        {user && user.kind === 'Manager'
          ? 'Painel interativo: Administrador'
          : user.kind === 'Supplier'
          ? 'Painel interativo: Fornecedor'
          : 'Seja bem vindo!!! Essa é sua área privada.'}
        <Navbar>
          <UserDropdownMenu />
        </Navbar>
      </Container>
    </Header>
  )
}

export default HeaderDash

const Header = styled.header`
.container{
  display: flex;
  align-items: center;
  justify-content: space-between;
  color: #ecc745;
  font-size: 1rem;
  min-height: 10vh;
  max-width: 80vw;
  @media (max-width: 768px) {
    max-width: 100vw;
  }
}
  
  background-color: black;
  min-height: 10vh;
 
  -webkit-box-shadow: 2px 2px 4px 1px rgba(0, 0, 0, 0.31);
  box-shadow: 2px 2px 4px 1px rgba(0, 0, 0, 0.31);
`
