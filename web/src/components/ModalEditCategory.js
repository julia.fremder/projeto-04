import React from 'react'
import { Card, CardHeader, CardBody, CardFooter, Button } from 'reactstrap'
import FormCategory from './FormCategory'
import styled from 'styled-components'
import { GlobalContext } from '../GlobalContext'

const ModalEditCategory = () => {
  const global = React.useContext(GlobalContext)
  const { fillForm, toggle, handleSubmit, form } = global

  return (
    <SCard className="edit-modal">
      <div className="modal-card">
        <CardHeader className="modal-header">
          Editar categoria:
          <Button className="close" onClick={() => toggle('Categories')}>
            x
          </Button>
        </CardHeader>
        <CardBody className="modal-body">
          <div>
            {form.name ? (
              <>
                Apague aqui todas as informações anteriores:{' '}
                <span>
                  <Button size="sm" onClick={() => fillForm()}>
                    apagar
                  </Button>
                </span>
              </>
            ) : (
              <>
                Clique aqui para preencher com as informações anteriores:{' '}
                <span>
                  <Button size="sm" onClick={() => fillForm()}>
                    preencher
                  </Button>
                </span>
              </>
            )}
          </div>

          <FormCategory />
        </CardBody>
        <CardFooter className="modal-footer">
          <Button
            size="sm"
            type="submit"
            onClick={() => handleSubmit('Category', form, form.id)}
          >
            Enviar
          </Button>
        </CardFooter>
      </div>
    </SCard>
  )
}

export default ModalEditCategory

const SCard = styled(Card)`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  margin-top: 1rem;
  background-color: transparent;
  border: none;
  color: #696969;

  .close {
    background-color: transparent !important;
    -webkit-box-shadow: none !important;
    box-shadow: none !important;
  }

  .form-control-file {
    font-size: 0.8rem;
    color: ${(global) => global.theme.colors.secondary};
    ::-webkit-file-upload-button {
      border-radius: 1.5rem;
      background-color: #ecc745;
      margin-bottom: 1rem;
      margin-right: 0.2rem;
      cursor: pointer;
      border: 0 solid transparent;
      -webkit-box-shadow: 2px 2px 4px 1px rgba(0, 0, 0, 0.31);
      box-shadow: 2px 2px 4px 1px rgba(0, 0, 0, 0.31);
      color: #696969;
      &:hover {
        background-color: #ccc;
      }
    }
  }
  .modal-card {
    -webkit-box-shadow: 2px 2px 4px 1px rgba(0, 0, 0, 0.31);
    box-shadow: 2px 2px 4px 1px rgba(0, 0, 0, 0.31);
    border: 0.5px solid #eee;
    width: 100%;
    max-width: 1000px;
  }
  .card-body {
    display: flex;
    flex-direction: column;
    height: 500px;
    background-color: #fff;
    padding: 2rem;
    img {
      width: 200px;
    }
  }
`
