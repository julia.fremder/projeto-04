import React from 'react'
import { GlobalContext } from '../GlobalContext'
import FooterComponent from './FooterComponent'
import HeaderHome from './HeaderHome'

const LayoutHome = ({ children }) => {
  return (
    <>
      <HeaderHome />
      {children}
      <FooterComponent />
    </>
  )
}

export default LayoutHome
