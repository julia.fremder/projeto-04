/* eslint-disable jsx-quotes */
/* eslint-disable react/jsx-no-undef */
import React from 'react'
import InputComponent from './InputSearchHome'
import styled from 'styled-components'
import imageUrl from '../assets/image/Oficina.JPG'

const BannerImage = () => {
  return (
    <Banner>
      <P>Encontre a Oficina ideal para si</P>
      <InputComponent />
    </Banner>
  )
}

export default BannerImage

const Banner = styled.div`
  background-image: url(${imageUrl});
  background-repeat: no-repeat;
  background-position: center;
  background-size: cover;
  object-fit: cover;
  height: 500px;
  width: 100vw;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`
const P = styled.p`
  font-size: 3rem;
  font-weight: bold;
  color: #fafafa;
  text-shadow: 1px 1px 2px black;
`
