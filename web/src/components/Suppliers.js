import React from 'react'
import {
  BsExclamationTriangleFill,
  BsCheckCircle,
  BsFillQuestionCircleFill
} from 'react-icons/bs'
import { IoOpenOutline } from 'react-icons/io5'
import { RiArrowDropUpFill } from 'react-icons/ri'
import { Row, Container, Button, Table } from 'reactstrap'
import styled from 'styled-components'
import { UncontrolledTooltip } from 'reactstrap'
import { GlobalContext } from '../GlobalContext'

const Suppliers = () => {
  const global = React.useContext(GlobalContext)
  const { toggle, suppliers, handleSubmit } = global
  return (
    <>
      <Button className="collapsed-btn" onClick={() => toggle('Manager')}>
        Ocultar Fornecedores
        <RiArrowDropUpFill />
      </Button>
      <SContainer className="list-paper">
        <Row>
          <Table striped bordered responsive>
            <thead>
              <tr>
                <th>Imagem</th>
                <th>Fornecedor</th>
                <th>NIPC</th>
                <th>Status</th>
                {/* <th>Detalhes</th> */}
              </tr>
            </thead>
            <tbody>
              {suppliers &&
                suppliers.map((supplier) => (
                  <tr
                    key={supplier.id}
                    className="supplier-details"
                    onClick={() =>
                      handleSubmit('DetailSupplier', null, supplier.id)
                    }
                  >
                    <td className="align-center" scope="row">
                      <div className="list-image">
                        <img
                          src={process.env.REACT_APP_API + supplier.image}
                        ></img>
                      </div>
                    </td>
                    <td className="align-row">{supplier.name}</td>
                    <td>{supplier.NIF}</td>
                    <td>
                      {supplier.status === 'Refused' ? (
                        <>
                          <BsExclamationTriangleFill
                            id="abc"
                            style={{ color: 'red' }}
                          />
                          <UncontrolledTooltip
                            style={{
                              fontSize: '0.7rem',
                              borderRadius: '7px',
                              backgroundColor: '#696969'
                            }}
                            placement="right"
                            target="abc"
                          >
                            Inativo
                          </UncontrolledTooltip>
                        </>
                      ) : supplier.status === 'Pending' ? (
                        <>
                          <BsFillQuestionCircleFill
                            id="pending"
                            style={{ color: '#ecc745' }}
                          />
                          <UncontrolledTooltip
                            style={{
                              fontSize: '0.7rem',
                              borderRadius: '7px',
                              backgroundColor: '#696969'
                            }}
                            placement="right"
                            target="pending"
                          >
                            Pendente
                          </UncontrolledTooltip>
                        </>
                      ) : (
                        <>
                          <BsCheckCircle
                            id="approved"
                            style={{ color: 'green' }}
                          />
                          <UncontrolledTooltip
                            style={{
                              fontSize: '0.7rem',
                              borderRadius: '7px',
                              backgroundColor: '#696969'
                            }}
                            placement="right"
                            target="approved"
                          >
                            Ativo
                          </UncontrolledTooltip>
                        </>
                      )}
                    </td>
                  </tr>
                ))}
            </tbody>
          </Table>
        </Row>
      </SContainer>
    </>
  )
}

export default Suppliers

const SContainer = styled(Container)`
  background-color: #fafafa;
 width: 70vw;
  padding: 20px 10px;
  -webkit-box-shadow: 2px 2px 4px 1px rgba(0, 0, 0, 0.31);
  box-shadow: 2px 2px 4px 1px rgba(0, 0, 0, 0.31);
  color: #696969;

  @media (max-width: 1024px) {
    width: 90vw;
    font-size: 1.5vw;
  }
  .list-image {
    width: 100px;
    max-height: 50px;
    display: flex;
    align-self: center;
    justify-self: center;
    img {
      overflow: auto;
    }
  }
  .table {
    text-align: center;
    vertical-align: middle;
    font-size: 0.9rem;
    .supplier-details {
      cursor: pointer;
    }
    td {
      text-align: center;
    }
    .btn {
      display: flex;
      flex-direction: row;
      align-items: center;
      justify-content: center;
    }
  }
  .adicionar-categoria {
    display: flex;
    justify-content: flex-start;
  }
`
