import React from 'react'
import { Form, FormGroup, Input, Label, Button } from 'reactstrap'
import { IoIosTrash } from 'react-icons/io'
import styled from 'styled-components'
import { GlobalContext } from '../GlobalContext'

const FormCategory = () => {
  const global = React.useContext(GlobalContext)
  const { handleChange, form, removeImg, preview, previewImg } = global
  return (
    <SForm>
      <FormGroup>
        <Label htmlFor="name">Nome</Label>
        <Input
          type="text"
          name="name"
          placeholder="Dê um nome para a categoria"
          onChange={handleChange}
          value={form.name || ''}
        />
      </FormGroup>
      <FormGroup>
        <Label htmlFor="description">Descrição</Label>
        <Input
          type="text"
          name="description"
          placeholder="Descreva a categoria"
          value={form.description || ''}
          onChange={handleChange}
        />
      </FormGroup>
      <div className="modal-image">
        <div className="square-image">
          <Button size="sm" onClick={() => removeImg()}>
            <IoIosTrash />
          </Button>
          {preview ? <img src={preview} /> : <p>Imagem</p>}
        </div>
      </div>
      <FormGroup>
        <Input
          type="file"
          name="image"
          accept="image/*"
          onChange={previewImg}
        />
      </FormGroup>
    </SForm>
  )
}

export default FormCategory

const SForm = styled(Form)`
  display: flex;
  flex-direction: column;
  flex: 1;

  .form-group {
    margin: 0.7rem 0;
    label {
      font-size: 0.8rem;
      color: #696969;
    }
    input[type='text'] {
      color: #696969;
      font-size: 0.95rem;
    }
    input[type='file'] {
      color: #696969;
    }
  }
  .modal-image {
    display: flex;
    flex-direction: column;
    .square-image {
      width: 200px;
      height: 200px;
      object-fit: cover;
      overflow: hidden;
      border: 0.3px solid #eee;
      position: relative;

      img {
        z-index: 0;
        height: 100%;
      }

      p {
        color: #696969;
        height: 100%;
        line-height: 150px;
        margin: 0;
        text-align: center;
      }
      .btn {
        display: flex;
        position: absolute;
        margin-top: 5px !important;
        align-items: center;
        justify-content: center;
        background-color: #fafafa !important;
        color: black;
        border: none;
        border-radius: 0px !important;
        z-index: 100;
        svg {
          font-size: 1.2rem;
          color: black;
        }
      }
    }
    /* .remove-image{
    width: 200px;
    height: 200px;
    object-fit: cover;
    overflow: hidden;
    display: flex;
    flex-direction: column;
    align-items: flex-end;
    justify-content: flex-end;
    padding-bottom:2px; */
  }
  /* } */
`
