import React from 'react'
import { ModalHeader, Modal, ModalBody, ModalFooter } from 'reactstrap'
import { BsExclamationTriangleFill, BsFillQuestionCircleFill } from 'react-icons/bs'
import { GlobalContext } from '../GlobalContext'
import styled from 'styled-components'

const SupplierMessage = () => {
  const global = React.useContext(GlobalContext)
  const { user, modal, toggle } = global
  return (
    <SModal isOpen={modal.status}>
      <ModalHeader toggle={() => toggle('Manager')}>
        Atenção{' '}
        {user && user.kind === 'Refused' ? (
          <BsExclamationTriangleFill id="abc" style={{ color: 'red' }} />
        ) : (
          <BsFillQuestionCircleFill id="pending" style={{ color: '#ecc745' }} />
        )}
      </ModalHeader>
      <ModalBody>{user && user.kind === 'Refused' ? (
         'Sentimos muito, o seu cadastro não foi aprovado.'
        ) : (
          'O seu cadastro está pendente de aprovação, aguarde o e-mail de confirmação.'
        )}</ModalBody>
      <ModalFooter>Envie e-mail para oficinadvisor@gmail.com para mais informações</ModalFooter>
    </SModal>
  )
}

export default SupplierMessage

const SModal = styled(Modal)`
.modal-header {
  .close {
    border: none;
    background-color: transparent;
    font-size: 1.25rem;
  }
}
.modal-footer {
  font-size: 0.8rem;
}
`