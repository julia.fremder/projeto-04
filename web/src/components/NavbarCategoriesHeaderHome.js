import React from 'react'
import { Container, Navbar, NavItem, NavLink } from 'reactstrap'
import styled from 'styled-components'

const NavbarCategoriesHeaderHome = () => {
  return (
    <Container>
      <SNavbar>
        <NavItem>
          <SNavLink>Mecânica</SNavLink>
        </NavItem>
        <NavItem>
          <SNavLink>Chapa e pintura</SNavLink>
        </NavItem>
        <NavItem>
          <SNavLink>Detail</SNavLink>
        </NavItem>
        <NavItem>
          <SNavLink>Pneus</SNavLink>
        </NavItem>
      </SNavbar>
    </Container>
  )
}

export default NavbarCategoriesHeaderHome

const SNavLink = styled(NavLink)`
  color: ${(props) => props.theme.colors.primary};
  cursor: pointer;
  &:hover {
    color: ${(props) => props.theme.colors.secondary};
  }
`
const SNavbar = styled(Navbar)`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
`
