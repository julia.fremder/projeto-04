/* eslint-disable jsx-quotes */
import React from 'react'
import styled from 'styled-components'
import { IoLogoFacebook, IoLogoInstagram, IoLogoWhatsapp } from 'react-icons/io5'
import Brand from './Brand'

const FooterComponent = () => {
  return (
    <Footer>
      <div className="footer-direito">
        <Brand />
        <p>Todos os direitos reservados</p>
      </div>
      <ul className="footer-links">
        <li>
          <a to="http://instagram.com"><IoLogoInstagram/></a>
        </li>
        <li>
          <a to="http://facebook.com"><IoLogoFacebook/></a>
        </li>
        <li>
          <a to="http://whatsapp.web.com"><IoLogoWhatsapp/></a>
        </li>
      </ul>
    </Footer>
  )
}

export default FooterComponent

 const Footer = styled.footer`
  min-height: 10rem;
  background-color: #eee;
      .logomarca {
          height: 30px !important;
          width: 100%;
          margin: 2rem 0;
      }
      .footer-direito {
          width: 100%;
          font-size: 0.8rem;
          line-height: 0.8px;
          p {
              width: 100%;
              margin-bottom: 3rem;
              text-align: center;
          }
      }
      .footer-links{
          display: flex;
          justify-content: center;
          width: 100%;
          a{
              margin-right: 1rem;
          }
      }
  
`
