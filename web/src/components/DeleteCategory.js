import React from 'react'
import { Card, CardHeader, CardBody, CardFooter, Button } from 'reactstrap'
import styled from 'styled-components'
import { GlobalContext } from '../GlobalContext'

const DeleteCategory = () => {
  const global = React.useContext(GlobalContext)
  const { toggle, details, deleteCategory } = global
  return (
    <SCard className="edit-modal">
      <div className="modal-card">
        <CardHeader className="modal-header">
          Excluir categoria:
          <Button className="close" onClick={() => toggle('Categories')}>
            x
          </Button>
        </CardHeader>
        <CardBody className="modal-body">
          <Detail>
            <div className="detail-info">
              <div className="detail-image">
                <img src={process.env.REACT_APP_API + details.image}></img>
              </div>
              <div>
                <p>
                  <strong>Categoria: </strong>
                  {details.name}
                </p>
                <p>
                  <strong>Descrição: </strong>
                  {details.description}
                </p>
              </div>
            </div>
          </Detail>
        </CardBody>
        <CardFooter className="modal-footer">
          <Button
            size="sm"
            type="submit"
            onClick={() => deleteCategory(details.id)}
          >
            Excluir
          </Button>
          <Button size="sm" type="submit" onClick={() => toggle('Categories')}>
            Voltar
          </Button>
        </CardFooter>
      </div>
    </SCard>
  )
}

export default DeleteCategory

const Detail = styled.section`
  display: flex;
  flex-direction: column;
  background-color: #fafafa;
  padding-top: 20px;
  padding-bottom: 20px;
  font-size: 0.9rem;
  margin: 0rem auto 0.5rem auto;

  @media (max-width: 1024px) {
    max-width: 100%;
    font-size: 1.5vw;
  }

  .detail-info {
    display: flex;
  }
  .detail-image {
    width: 10vw;
    height: 100px;
    overflow: hidden;
    display: flex;
    /* align-items: center; */
    justify-items: center;
    img {
      height: 60px;
      width: auto;
      margin-right: auto;
      margin-left: auto;
    }
  }
  .detail-footer {
    display: flex;
    justify-content: flex-end;
  }
`

const SCard = styled(Card)`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  margin-top: 1rem;
  background-color: transparent;
  border: none;
  color: #696969;
  max-width: 60%;

  .close {
    background-color: transparent !important;
    -webkit-box-shadow: none !important;
    box-shadow: none !important;
  }

  .form-control-file {
    font-size: 0.8rem;
    color: ${(props) => props.theme.colors.secondary};
    ::-webkit-file-upload-button {
      border-radius: 1.5rem;
      background-color: #ecc745;
      margin-bottom: 1rem;
      margin-right: 0.2rem;
      cursor: pointer;
      border: 0 solid transparent;
      -webkit-box-shadow: 2px 2px 4px 1px rgba(0, 0, 0, 0.31);
      box-shadow: 2px 2px 4px 1px rgba(0, 0, 0, 0.31);
      color: #696969;
      &:hover {
        background-color: #ccc;
      }
    }
  }
  .modal-card {
    -webkit-box-shadow: 2px 2px 4px 1px rgba(0, 0, 0, 0.31);
    box-shadow: 2px 2px 4px 1px rgba(0, 0, 0, 0.31);
    border: 0.5px solid #eee;
    width: 100%;
    max-width: 1000px;
  }
  .card-body {
    display: flex;
    flex-direction: column;
    height: 300px;
    background-color: #fff;
    padding: 2rem;
    img {
      width: 200px;
    }
  }
`
