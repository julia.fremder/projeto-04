import React from 'react'
import { Container, Row } from 'reactstrap'
import styled from 'styled-components'

import { GlobalContext } from '../GlobalContext'

const Manager = () => {
  const global = React.useContext(GlobalContext)
  const { detailComponent, modal } = global

  return <SContainer>{detailComponent(modal.section)}</SContainer>
}

export default Manager

const SContainer = styled(Container)`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  height: 100%;
  /* .collapsed-btn {
      margin: 3rem auto 0.5rem auto !important;
      background-color: #ecc745;
      color: black;
      height: 3rem;
      border: 0px solid transparent;
      border-radius: 0.1rem !important;
      max-width: 60%;
      -webkit-box-shadow: 2px 2px 4px 1px rgba(0, 0, 0, 0.31);
      box-shadow: 2px 2px 4px 1px rgba(0, 0, 0, 0.31);
      svg {
        font-size: 1.3rem;
      }
      @media (max-width: 1024px) {
        max-width: 100%;
      }
    }
    .collapsed-btn:hover {
      background-color: #eee;
      color: black;
      font-weight: bold;
      font-size: 1.1rem;
    } */
`
