/* eslint-disable jsx-quotes */
import React from 'react'
import AdvertiseComponent from './AdvertiseComponent'
import Brand from './Brand'
import styled from 'styled-components'
import { IoIosSearch } from 'react-icons/io'
import NavbarComponent from './NavbarCategoriesHeaderHome'
import { Container, Navbar, Button } from 'reactstrap'
import ModalSignIn from './ModalSignInHome'
import { GlobalContext } from '../GlobalContext'

const HeaderHome = () => {
  const global = React.useContext(GlobalContext)
  const { toggle } = global

  return (
    <Container>
      <ModalSignIn />
      <AdvertiseComponent />
      <Header>
        <Navbar>
        <Brand />
        </Navbar>
        <Input>
          <IoIosSearch />
          <SInput type="text" />
        </Input>
        <Button className="btn-amarelo" onClick={() => toggle('SignIn')}>Iniciar Sessão</Button>
      </Header>
      <NavbarComponent />
    </Container>
  )
}

export default HeaderHome

const Header = styled.header`
  display: flex;
  margin: auto 5vw;
  align-items: center;
  justify-content: space-between;
  margin-left: 0;
  margin-right: 0;
`

const Input = styled.div`
  display: flex;
  align-items: center;
  height: 2.5rem;
  flex: 1;
  border: 0.3px solid #eee;
  border-radius: 2rem;
  margin-right: 2rem;
  svg {
    padding: 0.2rem;
    font-size: 2rem;
  }
`

const SInput = styled.input`
  border: none;
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`
