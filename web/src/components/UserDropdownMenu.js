import React from 'react'
import {
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from 'reactstrap'
import { IoExitOutline } from 'react-icons/io5'
import styled from 'styled-components'
import { GlobalContext } from '../GlobalContext'

const UserDropdownMenu = () => {
  const global = React.useContext(GlobalContext)
  const { dropdownOpen, toggleUserMenu, handleClick, user } = global
  return (
    <SDropdown isOpen={dropdownOpen} toggle={() => toggleUserMenu()}>
      <DropdownToggle className="btn-amarelo" caret>{user.name}</DropdownToggle>
      <DropdownMenu>
        <DropdownItem header>Perfil</DropdownItem>
        <DropdownItem divider />
        <DropdownItem onClick={handleClick}>
          Sair <IoExitOutline />
        </DropdownItem>
      </DropdownMenu>
    </SDropdown>
  )
}

export default UserDropdownMenu

const SDropdown = styled(Dropdown)`
  .dropdown-item {
    font-size: 0.9rem;
    margin-right: 1rem;
  }
`
