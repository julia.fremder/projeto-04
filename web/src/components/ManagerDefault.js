import React from 'react'
import { Button } from 'reactstrap'
import { RiArrowDropDownFill } from 'react-icons/ri'
import { GlobalContext } from '../GlobalContext'

const ManagerDefault = () => {
  const global = React.useContext(GlobalContext)
  const { toggle, user } = global
  return (
    <>
      {user && user.kind === 'Manager' ? (
        <>
          {' '}
          <Button
            className="collapsed-btn"
            onClick={() => toggle('Categories')}
          >
            Categorias
            <RiArrowDropDownFill />
          </Button>
          <Button className="collapsed-btn" onClick={() => toggle('Suppliers')}>
            Fornecedores
            <RiArrowDropDownFill />
          </Button>
        </>
      ) : (
        ''
      )}
    </>
  )
}

export default ManagerDefault
