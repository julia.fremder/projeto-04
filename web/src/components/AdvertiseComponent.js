import React from 'react'
import { Container } from 'reactstrap'
import styled from 'styled-components'
import imageUrl from '../assets/image/advertise.jpg'

const AdvertiseComponent = () => {
  return (
    <Advertise />
  )
}

export default AdvertiseComponent

const Advertise = styled(Container)`
  height: 6rem;
  display: flex;
  align-items: center;
  justify-content: center;
  margin: 2rem auto;
  border: 0.5px solid grey;
  width: 60%;
  background-image: url(${imageUrl});
  background-repeat: no-repeat;
  background-position: center;
  background-size: cover;
  object-fit: cover;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`
