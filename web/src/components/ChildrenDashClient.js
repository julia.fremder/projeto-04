import React from 'react'
import { Container, Row, Col } from 'reactstrap'
import BannerImage from '../components/BannerImage'
import BannerCards from '../components/BannerCards'
import { GlobalContext } from '../GlobalContext'

const Client = () => {
  const global = React.useContext(GlobalContext)
  const { user } = global
  return (
    <>
      <Container>
        <Row>
          <Col>
            Client {user && user.id} {user && user.name}
          </Col>
        </Row>
      </Container>
      <BannerImage />
      <BannerCards />
    </>
  )
}

export default Client
