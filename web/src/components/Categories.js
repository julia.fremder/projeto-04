import React from 'react'
import { IoIosTrash } from 'react-icons/io'
import { IoOpenOutline } from 'react-icons/io5'
import { RiArrowDropUpFill } from 'react-icons/ri'
import { FaRegEdit } from 'react-icons/fa'
import { Row, Container, Table, Button } from 'reactstrap'
import styled from 'styled-components'
import { GlobalContext } from '../GlobalContext'

const Categories = () => {
  const global = React.useContext(GlobalContext)
  const { toggle, modal, categories, clickCategory } = global
  return (
    <>
        <Button className="collapsed-btn" onClick={() => toggle('Manager')}>
          Ocultar Categorias
          <RiArrowDropUpFill />{' '}
        </Button>

      <SContainer>
        <Table striped bordered>
          <thead>
            <tr>
              <th className="align-center">Imagem</th>
              <th>Categoria</th>
              <th>Descrição</th>
              <th className="align-center">Ações</th>
            </tr>
          </thead>
          <tbody>
            {modal.section !== 'Detail' &&
              categories &&
              categories.map((category) => (
                <tr key={category.id}>
                  <td className="align-center" scope="row">
                    <div className="list-image">
                      <img
                        src={process.env.REACT_APP_API + category.image}
                      ></img>
                    </div>
                  </td>
                  <td className="align-row">{category.name}</td>
                  <td>{category.description}</td>
                  <td>
                    <div className="d-flex">
                      <Button
                        className="btn-amarelo"
                        onClick={() => clickCategory('Detail', category.id)}
                      >
                        <IoOpenOutline />
                      </Button>
                      <Button
                        className="btn-amarelo"
                        onClick={() => clickCategory('Edit', category.id)}
                      >
                        <FaRegEdit />
                      </Button>
                      <Button
                        className="btn-amarelo"
                        onClick={() => clickCategory('Delete', category.id)}
                      >
                        <IoIosTrash />
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
          </tbody>
        </Table>
        <div className="adicionar-categoria">
          <Button className="btn-amarelo" onClick={() => toggle('AddCategory')}>
            Adicionar categoria
          </Button>
        </div>
      </SContainer>
    </>
  )
}

export default Categories

const SContainer = styled(Container)`
  background-color: #fafafa;
  width: 90vw !important;
  margin: 0 !important;
  padding: 1rem;
  -webkit-box-shadow: 2px 2px 4px 1px rgba(0, 0, 0, 0.31);
  box-shadow: 2px 2px 4px 1px rgba(0, 0, 0, 0.31);
  color: #696969;

  @media (min-width: 768px) {
    width: 70vw !important;
    font-size: 1.5vw;
  }
  .list-image {
    width: 40px;
    max-height: 40px;
    overflow: hidden;
    display: flex;
    align-self: center;
    justify-self: center;
    img {
      height: 40px;
      width: auto;
    }
  }
  .table {
    text-align: center;
    vertical-align: middle;
    font-size: 0.9rem;
    overflow-x: auto;
  }
  .adicionar-categoria {
    display: flex;
    justify-content: flex-start;
  }
`
