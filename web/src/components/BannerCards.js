import React from 'react'
import styled from 'styled-components'
import {
  Container,
  Card,
  CardImg,
  CardTitle,
  CardBody,
  CardFooter
} from 'reactstrap'
import { GlobalContext } from '../GlobalContext'

const BannerCards = () => {
  const global = React.useContext(GlobalContext)
  const { suppliers } = global
  return (
    <>
      <Section>
        <Container>
          {suppliers &&
            suppliers
              .filter(({ status }) => status === 'Approved')
              .map((supplier) => (
                <Card key={supplier.id}>
                  <CardBody>
                    <CardImg src={process.env.REACT_APP_API + supplier.image} />
                    <div className="supplier-info">
                      <CardTitle>{supplier.name}</CardTitle>
                      Telefone: {supplier.phone}
                      <br />
                      NIF: {supplier.NIF}
                    </div>
                  </CardBody>

                  <CardFooter>
                    {supplier.street} {supplier.zipcode}
                  </CardFooter>
                </Card>
              ))}
        </Container>
      </Section>
    </>
  )
}

export default BannerCards

const Section = styled.section`
  background-color: ${(props) => props.theme.colors.secondary};
  width: 100vw;
  margin-top: 5rem;
  .container {
    display: flex;
    justify-content: center;
    flex-wrap: wrap;
    .card {
      width: 300px;
      margin: 1rem;
      padding: 0.5rem;
      background-color: #eee;
      .card-body {
        display: flex;
        padding: 0;
        background-color: #414040;
        color: #eee;
        font-size: 0.8rem;
        .supplier-info {
          display: flex;
          flex-direction: column;
          flex: 1;
          padding: 0.5rem;
          justify-content: center;
        }
        .card-title {
          font-size: 1.2rem;
        }
        img {
          width: 100px;
          height: auto;
          margin: 1rem;
          object-fit: contain;
          background-color: #fafafa;
        }
      }

      .card-footer {
        font-size: 0.8rem;
        padding: 0;
      }
    }
  }
`
