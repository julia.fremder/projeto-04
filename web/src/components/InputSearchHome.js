import React from 'react'
import { IoIosSearch } from 'react-icons/io'

import styled from 'styled-components'
import { GlobalContext } from '../GlobalContext'

const InputSearchHome = () => {
const global = React.useContext(GlobalContext)
const { handleChange, form } = global
  return (
    <Div>
      <IoIosSearch />
      <Input
        onChange={handleChange}
        name="search"
        type="search"
        placeholder="pesquise sua oficina"
        value={form?.search || ''}
      />
    </Div>
  )
}

export default InputSearchHome

const Div = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  background-color: #fafafa;
  border-radius: 2rem;
  padding: 0 1rem;
  width: 70%;
  height: 3rem;
  box-shadow: 1px 1px 2px grey;
  svg {
    font-size: 2rem;
    margin-right: 1rem;
  }
`
const Input = styled.input`
  background-color: #fafafa;
  border: none;
  height: 3rem;
`
