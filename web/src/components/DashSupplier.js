import React from 'react'
import { IoIosTrash } from 'react-icons/io'
import styled from 'styled-components'
import { Button, Container, Row, Table } from 'reactstrap'
import { GlobalContext } from '../GlobalContext'

const DashSupplier = () => {
  const global = React.useContext(GlobalContext)
  const { toggle, autoservices, user, categories, deleteAutoservice } = global
  return (
    <>
      {user && user.kind === 'Supplier' ? (
        <SContainer className="list-paper">
          <div className="autoservices-list">
            <div className="autoservices-title">Serviços Cadastrados</div>
            <Table striped bordered responsive>
              <thead>
                <tr>
                  <th>Categoria</th>
                  <th className="align-center">Imagem</th>
                  <th>Serviço</th>
                  <th>Preço</th>
                  <th className="align-center">Ações</th>
                </tr>
              </thead>
              <tbody>
                {autoservices &&
                  autoservices.map((service) => (
                    <tr key={service.id}>
                      <td>
                        {categories &&
                          categories.find(
                            (category) => category.id === service.category_id
                          )?.name}
                      </td>
                      <td className="align-center" scope="row">
                        <div className="list-image">
                          <img
                            src={process.env.REACT_APP_API + service.image}
                          ></img>
                        </div>
                      </td>
                      <td className="align-row">{service.name}</td>
                      <td>{service.cost}</td>
                      <td>
                        <div className="d-flex">
                          <Button
                            className="btn-amarelo"
                            onClick={() =>
                              deleteAutoservice(user.id, service.id)
                            }
                          >
                            <IoIosTrash />
                          </Button>
                        </div>
                      </td>
                    </tr>
                  ))}
              </tbody>
            </Table>
          </div>

          <div className="adicionar-categoria">
            <Button
              className="btn-amarelo"
              onClick={() => toggle('AddAutoservice')}
            >
              Adicionar serviço
            </Button>
          </div>
        </SContainer>
      ) : (
        ''
      )}
    </>
  )
}
export default DashSupplier

const SContainer = styled(Container)`
  display: flex;
  flex-direction: column;
  max-width: 70vw;
  align-items: center;
  padding: 5vw;
  background-color: #fafafa;
  margin: 3rem auto;
  padding: 20px 10px;
  -webkit-box-shadow: 2px 2px 4px 1px rgba(0, 0, 0, 0.31);
  box-shadow: 2px 2px 4px 1px rgba(0, 0, 0, 0.31);
  color: #696969;

  @media (max-width: 1024px) {
    max-width: 100vw;
    font-size: 1.5vw;
  }

  .autoservices-list {
    width: 100%;
    .autoservices-title {
      background-color: ${(props) => props.theme.colors.secondary};
      padding: 1rem;
      color: ${(props) => props.theme.colors.primary};
      margin-bottom: 1rem;
      -webkit-box-shadow: 2px 2px 4px 1px rgba(0, 0, 0, 0.31);
      box-shadow: 2px 2px 4px 1px rgba(0, 0, 0, 0.31);
      color: #696969;
    }

    .table {
      text-align: center;
      vertical-align: middle;
      font-size: 0.9rem;
      .list-image {
        width: 40px;
        max-height: 40px;
        display: flex;
        align-self: center;
        justify-self: center;
        img {
          overflow: auto;
        }
      }
      .d-flex {
        display: flex;
        justify-content: center;
      }
    }
  }
  .adicionar-categoria {
    width: 100%;
    display: flex;
    justify-content: flex-start;
  }
`
