/* eslint-disable jsx-quotes */
import React from 'react'
import imageUrl from '../assets/image/oficinadvisor2.svg'

const Brand = () => {
  return (
      <img src={imageUrl} alt="logomarca" className="logomarca" />
  )
}

export default Brand

