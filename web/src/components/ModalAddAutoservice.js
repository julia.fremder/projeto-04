import React from 'react'
import { Modal, ModalBody, ModalFooter, ModalHeader, Button } from 'reactstrap'
import FormCategory from './FormCategory'
import styled from 'styled-components'
import { GlobalContext } from '../GlobalContext'
import FormAutoservice from './FormAutoservice'

const ModalAddCategory = () => {
  const global = React.useContext(GlobalContext)
  const { modal, toggle, handleSubmit, removeImg, user, form } = global
  return (
    <SModal
      isOpen={
        modal && modal.section === 'AddAutoservice' ? modal.status : false
      }
    >
      <ModalHeader toggle={() => toggle('Supplier')}>Novo serviço:</ModalHeader>
      <ModalBody>
        <FormAutoservice />
      </ModalBody>
      <ModalFooter>
        <Button
          type="submit"
          onClick={() => handleSubmit('AddAutoservice', form, user.id)}
        >
          adicionar
        </Button>
        <Button onClick={() => removeImg()}>remover imagem</Button>
      </ModalFooter>
    </SModal>
  )
}

export default ModalAddCategory

const SModal = styled(Modal)`
  .modal-header {
    .close {
      border: none;
      background-color: transparent;
      font-size: 1.25rem;
    }
  }

  .modal-body {
    padding: 1rem;
  }
`
