import React from 'react'
import {
  Button,
  Input,
  FormGroup,
  Label,
  Row,
  Container,
  Table,
  UncontrolledTooltip
} from 'reactstrap'
import {
  BsExclamationTriangleFill,
  BsCheckCircle,
  BsFillQuestionCircleFill
} from 'react-icons/bs'
import { RiArrowDropUpFill } from 'react-icons/ri'
import styled from 'styled-components'
import { GlobalContext } from '../GlobalContext'

const DetailsSupplier = () => {
  const global = React.useContext(GlobalContext)
  const { toggle, detailsSupplier, changeStatus, autoservices } = global
  return (
    <>
      <Button
        className="collapsed-btn"
        onClick={() => global.toggle('Manager')}
      >
        Ocultar Fornecedores
        <RiArrowDropUpFill />
      </Button>
      <Detail>
        <div className="detail-info">
          <div className="detail-image">
            <img src={process.env.REACT_APP_API + detailsSupplier.image}></img>
          </div>
          <div className="supplier-card">
            <Table>
              <tbody >
                <tr>
                  <td className="supplier-data">{detailsSupplier.name}</td>
                </tr>
                <tr>
                  <td>
                    {detailsSupplier.street}
                    {', '}
                    {detailsSupplier.zipcode}
                  </td>
                </tr>
                <tr>
                  <td>{detailsSupplier.email}</td>
                </tr>
                <tr>
                  <td>{detailsSupplier.phone}</td>
                </tr>
              </tbody>
            </Table>
          </div>
          <div className="status-radio">
            <FormGroup tag="fieldset">
              <FormGroup check>
                <Label check>
                  <Input
                    type="radio"
                    onChange={() =>
                      changeStatus(detailsSupplier.id, 'Approved')
                    }
                    value="Approved"
                    checked={detailsSupplier.status === 'Approved'}
                    name="radio1"
                  />
                  Ativo
                </Label>
              </FormGroup>
              <FormGroup check>
                <Label check>
                  <Input
                    type="radio"
                    onChange={() => changeStatus(detailsSupplier.id, 'Refused')}
                    checked={detailsSupplier.status === 'Refused'}
                    name="radio1"
                    value="Refused"
                  />
                  Inativo
                </Label>
              </FormGroup>
              <FormGroup check disabled>
                <Label check>
                  <Input
                    type="radio"
                    value="Pending"
                    checked={detailsSupplier.status === 'Pending'}
                    name="radio1"
                    disabled
                  />{' '}
                  Pendente
                </Label>
              </FormGroup>
            </FormGroup>
          </div>
        </div>
        <div className="detail-footer">
          <Button className="btn-amarelo" onClick={() => toggle('Suppliers')}>
            Voltar
          </Button>
        </div>
      </Detail>
      <SContainer className="list-paper">
        {autoservices && autoservices.length > 0 ? (
          <Table striped bordered responsive>
            <thead>
              <tr>
                <th>Imagem</th>
                <th>Serviço</th>
                <th>Preço</th>
              </tr>
            </thead>
            <tbody>
              {autoservices &&
                autoservices.map((autoservice) => (
                  <tr
                    key={autoservice.id}
                    className="supplier-details"
                    onClick={() =>
                      handleSubmit('DetailSupplier', null, autoservice.id)
                    }
                  >
                    <td className="align-center" scope="row">
                      <div className="list-image">
                        <img
                          src={process.env.REACT_APP_API + autoservice.image}
                        ></img>
                      </div>
                    </td>
                    <td className="align-row">{autoservice.name}</td>
                    <td>{autoservice.cost}</td>
                  </tr>
                ))}
            </tbody>
          </Table>) : 'Não há serviços cadastrados pelo fornecedor.'}

      </SContainer>
    </>
  )
}

export default DetailsSupplier

const Detail = styled(Container)`
  display: flex;
  flex-direction: column;
  background-color: #fafafa;
  max-width: 60%;
  padding-top: 20px;
  padding-bottom: 20px;
  font-size: 0.9rem;
  margin: 0rem auto 0.5rem auto;
  -webkit-box-shadow: 2px 2px 4px 1px rgba(0, 0, 0, 0.31);
  box-shadow: 2px 2px 4px 1px rgba(0, 0, 0, 0.31);
  color: #696969;

  @media (max-width: 1024px) {
    max-width: 100%;
    font-size: 1.5vw;
  }

  .detail-info {
    display: flex;

    .supplier-card {
      padding: 0 1rem;
      td {
        padding: 0 0.5rem;
      }
    }

    .status-radio {
      margin-left: auto;
    }
  }
  .detail-image {
    width: 100px;
    height: 100px;
    box-sizing: border-box;
    display: flex;
    align-items: center;
    justify-items: center;
    img {
      overflow: auto;
    }
  }
  .detail-footer {
    display: flex;
    justify-content: flex-end;
  }
`
const SContainer = styled(Container)`
  background-color: #fafafa;
  max-width: 60%;
  margin-left: auto;
  margin-right: auto;
  padding: 20px 10px;

  @media (max-width: 1024px) {
    max-width: 100%;
    font-size: 1.5vw;
  }
  .list-image {
    width: 40px;
    max-height: 40px;
    display: flex;
    align-self: center;
    justify-self: center;
    img {
      overflow:auto;
    }
  }
  .table {
    text-align: center;
    vertical-align: middle;
    font-size: 0.9rem;
    .supplier-details {
      cursor: pointer;
    }
    td {
      text-align: center;
    }
    .btn {
      display: flex;
      flex-direction: row;
      align-items: center;
      justify-content: center;
    }
  }
  .adicionar-categoria {
    display: flex;
    justify-content: flex-start;
  }
`
