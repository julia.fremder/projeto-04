import React from 'react'
import FooterComponent from '../components/FooterComponent'
import HeaderDash from '../components/HeaderDash'


const LayoutDash = ({children}) => {
  return (
   <>
      <HeaderDash />
      <main className="paper">{children}</main>
      <FooterComponent />
   </>
  )
}
export default LayoutDash
