import { removeToken, saveAuth, TOKEN_KEY } from '../../config/storage'
import { authService } from '../../services/auth.service'
import { navigate } from '@reach/router'
import { toastr } from 'react-redux-toastr'

import http from '../../config/http'
import TYPES from '../types'

export const signInAction = (data) => {
  return async (dispatch) => {
    dispatch({ type: TYPES.SIGN_LOADING, status: true })
    try {
      const result = await authService(data)
      if (result.data.data) {
        saveAuth(result.data.data)
        http.defaults.headers.token = result.data.data.token
      }
      dispatch({
        type: TYPES.SIGN_IN,
        data: result.data.data
      })
      toastr.success(`${result.data.data.userServiceDTO.name}`, 'Autenticado com sucesso!')
      navigate('/dashboard')
    } catch (error) {      
      dispatch({ type: TYPES.SIGN_ERROR, data: error })
      navigate('/')
    }
  }
}

export const signOutAction = () => {
  return async (dispatch) => {
    dispatch({ type: TYPES.SIGN_LOADING, status: true })
    try {
      removeToken()
      dispatch({ type: TYPES.SIGN_OUT })
      navigate('/')
    } catch (error) {
      dispatch({ type: TYPES.SIGN_ERROR, data: error })
      navigate('/')
    }
  }
}
