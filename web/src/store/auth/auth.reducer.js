import TYPES from '../types'
import { getToken, getUser } from '../../config/storage'

const INITIAL_STATE = {
  loading: false,
  token: getToken() || '',
  user: getUser() || {},
  errors: {},
  status: {}
}

const reducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case TYPES.SIGN_LOADING:
      state.error = []
      state.loading = action.status
      return state
    case TYPES.SIGN_IN:
      state.token = action.data.token && action.data.token
      state.user = action.data.userServiceDTO && action.data.userServiceDTO
      state.status = action.data && action.data
      state.loading = false
      return state
    case TYPES.SIGN_UP:
      state.token = action.token
      state.user = action.userServiceDTO
      state.loading = false
      return state
    case TYPES.SIGN_ERROR:
      state.loading = false
      return {
        ...state,
        loading: false,
        token: '',
        user: {},
        status: {},
        errors: action.data
      }
    case TYPES.SIGN_OUT:
      state.token = ''
      state.user = {}
      state.error = []
      return state
    default:
      return state
  }
}

export default reducer
