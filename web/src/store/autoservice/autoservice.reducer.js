import TYPES from '../types'

const INITIAL_STATE = {
  loading: false,
  autoservices: [],
  details: {},
}

const reducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case TYPES.AUTOSERVICE_LOADING:
      state.loading = action.status
      return state
    case TYPES.AUTOSERVICE_LIST:
      state.loading = false
      return {  
        ...state,
        autoservices: [...action.data],// new AUTOSERVICES array
       }
    case TYPES.AUTOSERVICE_DETAILS:
      state.details = action.data
      state.loading = false
      return state
    default:
      return state
  }
}

export default reducer