import TYPES from '../types'
import {
  getAutoservices,
  deleteAutoservices,
  addAutoservice
} from '../../services/autoservice.service'
import { toastr } from 'react-redux-toastr'

export const addNewAutoservice = (form, id) => {
  return async (dispatch) => {
    const config = {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    }
    dispatch({
      type: TYPES.AUTOSERVICE_LOADING,
      status: true
    })
    try {
      const formNewAutoservice = new FormData()
      Object.keys(form).map((itemForm) =>
        formNewAutoservice.append(itemForm, form[itemForm])
      )
      const result = await addAutoservice(formNewAutoservice, id, config)
      dispatch({
        type: TYPES.AUTOSERVICE_LOADING,
        status: false
      })

      // dispatch({ type: TYPES.SUPPLIER_CREATE, data: result.data })
      toastr.success('Serviço Novo', 'O novo serviço foi cadastrado.')
      // dispatch(getCategoriesList())
    } catch (error) {
      dispatch({ type: TYPES.AUTOSERVICE_LOADING, status: false })
      toastr.error('erro', 'O serviço nao foi cadastrado.')
    }
  }
}

export const getAutoservicesList = (query = null) => {
  return async (dispatch) => {
    dispatch({ type: TYPES.AUTOSERVICE_LOADING, status: true })
    try {
      const listDB = await getAutoservices(query)

      dispatch({ type: TYPES.AUTOSERVICE_LIST, data: listDB.data.data })
    } catch (error) {
      dispatch({ type: TYPES.AUTOSERVICE_LOADING, status: false })
    }
  }
}

export const deleteAutoserviceById = (supplier_id, autoservice_id) => {
  return async (dispatch) => {
    dispatch({ type: TYPES.AUTOSERVICE_LOADING, status: true })
    try {
      await deleteAutoservices(supplier_id, autoservice_id)
      toastr.error('Serviço Excluído', 'O fornecedor excluiu um serviço.')
    } catch (error) {
      dispatch({ type: TYPES.AUTOSERVICE_LOADING, status: false })
    }
  }
}
