import TYPES from '../types'
import {
  addSupplier,
  getSuppliers,
  getSupplierById,
  refuseSupplier,
  approveSupplier
} from '../../services/supplier.service'
import { toastr } from 'react-redux-toastr'

export const addNewSupplier = (form) => {
  return async (dispatch) => {
    const config = {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    }
    dispatch({
      type: TYPES.SUPPLIER_LOADING,
      status: true
    })
    try {
      const formNewSupplier = new FormData()
      Object.keys(form).map((itemForm) =>
        formNewSupplier.append(itemForm, form[itemForm])
      )
      const result = await addSupplier(formNewSupplier, config)
      dispatch({
        type: TYPES.SUPPLIER_LOADING,
        status: false
      })
      toastr.success(
        'Cadastro',
        'Foi cadastrado com sucesso, aguarde e-mail de aprovação.'
      )
    } catch (error) {
      dispatch({ type: TYPES.SUPPLIER_LOADING, status: false })
      dispatch({type: TYPES.SIGN_ERROR, data: error })
      throw 'erro'
    }
  }
}

export const getSuppliersList = () => {
  return async (dispatch) => {
    dispatch({ type: TYPES.SUPPLIER_LOADING, status: true })
    try {
      const listDB = await getSuppliers()
      dispatch({ type: TYPES.SUPPLIER_LIST, data: listDB.data.data })
    } catch (error) {
      dispatch({ type: TYPES.SUPPLIER_LOADING, status: false })
    }
  }
}

export const getDataSupplierById = (id) => {
  return async (dispatch) => {
    dispatch({
      type: TYPES.SUPPLIER_LOADING,
      status: true
    })
    try {
      const result = await getSupplierById(id)
      dispatch({ type: TYPES.SUPPLIER_DETAILS, data: result.data.data })
    } catch (error) {
      dispatch({ type: TYPES.SUPPLIER_LOADING, status: false })
    }
  }
}

export const deleteSupplierById = (Supplier_id) => {
  return async (dispatch) => {
    dispatch({ type: TYPES.SUPPLIER_LOADING, status: true })
    try {
      await deleteSupplier(Supplier_id)
      dispatch(getSuppliersList())
    } catch (error) {
      dispatch({ type: TYPES.SUPPLIER_LOADING, status: false })
    }
  }
}

export const updateStatusSupplier = (id, status) => {
  return async (dispatch) => {
    let result
    try {
      if (status === 'Approved') {
        result = await approveSupplier(id)
        toastr.success(
          `${result.data.data.data.name}`,
          'O fornecedor está Ativo.'
        )
      } else {
        result = await refuseSupplier(id)
        toastr.error(
          `${result.data.data.data.name}`,
          'O fornecedor foi BLOQUEADO.'
        )
      }
      dispatch(getSuppliersList())
    } catch (error) {

    }
  }
}
