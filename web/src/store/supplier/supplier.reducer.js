import TYPES from '../types'

const INITIAL_STATE = {
  loading: false,
  suppliers: [],
  details: {},
  edited: {}
}

const reducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case TYPES.SUPPLIER_LOADING:
      state.loading = action.status
      return state
    case TYPES.SUPPLIER_LIST:
      state.loading = false
      return {  
        ...state,
        suppliers: [...action.data],// new suppliers array
       }
    case TYPES.SUPPLIER_DETAILS:
      state.details = action.data
      state.loading = false
      return state
    case TYPES.SUPPLIER_EDIT:
      state.edited = action.data
      return state
    default:
      return state
  }
}

export default reducer