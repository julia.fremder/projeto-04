import TYPES from '../types'
import { addCategory, deleteCategory, getCategories, updateCategory, getCategoryById } from '../../services/category.service'
import { toastr } from 'react-redux-toastr'

export const addNewCategory = (form) => {
  return async (dispatch) => {
    const config = {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    }
    dispatch({
      type: TYPES.CATEGORY_LOADING,
      status: true
    })
    try {
      const formNewCategory = new FormData()
      Object.keys(form).map((itemForm) => formNewCategory.append(itemForm, form[itemForm]))  
      const result = await addCategory(formNewCategory, config)
      dispatch({ type: TYPES.CATEGORY_CREATE, data: result.data })
      toastr.success('Categoria', 'Categoria cadastrada com sucesso')
      // dispatch(getCategoriesList())
    } catch (error) {
      dispatch({type: TYPES.CATEGORY_LOADING, status: false})
      throw 'erro'
    }
  }
}

export const getCategoriesList = () => {
  return async (dispatch) => {
    dispatch ({ type: TYPES.CATEGORY_LOADING, status: true})
    try {
      const listDB = await getCategories()
      dispatch({ type: TYPES.CATEGORY_LIST, data: listDB.data.data})
    } catch (error) {
      dispatch({ type: TYPES.CATEGORY_LOADING, status:false})      
    }
  }
}

export const getDataCategoryById = (id) => {
  return async (dispatch) => {
    dispatch({
      type: TYPES.CATEGORY_LOADING,
      status: true
    })
    try {
      // const { auth } = getState()
      const result = await getCategoryById(id)
  
      dispatch({ type: TYPES.CATEGORY_DETAILS, data: result.data})
    } catch (error) {

      dispatch({ type: TYPES.CATEGORY_LOADING, status:false})
    }
  }
}

export const updateCategoryById = (id, form) => {
  return async (dispatch) => {
    const config = {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    }
    dispatch({
      type: TYPES.CATEGORY_LOADING,
      status: true
    })
    try {
      const formNewCategory = new FormData()
      Object.keys(form).map((itemForm) => formNewCategory.append(itemForm, form[itemForm]))  
      const result = await updateCategory(id, formNewCategory, config)
      // dispatch({ type: TYPES.CATEGORY_CREATE, data: result.data })
      toastr.success('Categoria', 'Categoria atualizada com sucesso')
    } catch (error) {
      dispatch({type: TYPES.CATEGORY_LOADING, status: false})
    }
  }
}

export const deleteCategoryById = (category_id) => {
  return async (dispatch) => {
    dispatch({type: TYPES.CATEGORY_LOADING, status: true})
    try{
      await deleteCategory(category_id)
      dispatch(getCategoriesList())
    } catch (error) {
      dispatch({type: TYPES.CATEGORY_LOADING, status: false})
    }
  }
}

export const editDetails = (category_id) => {
  return async (dispatch) => {
    try {
      const result = await getCategoryById(category_id)

      dispatch({ type: TYPES.CATEGORY_EDIT, data: result.data })
    } catch (error) {
      toastr.error('aconteceu um erro', error)
    }
  }
}