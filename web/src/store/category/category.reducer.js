import TYPES from '../types'

const INITIAL_STATE = {
  loading: false,
  categories: [],
  details: {},
  edited: {}
}

const reducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case TYPES.CATEGORY_LOADING:
      state.loading = action.status
      return state
    case TYPES.CATEGORY_LIST:
      state.loading = false
      return {  
        ...state,
        categories: [...action.data],// new categories array
       }
    case TYPES.CATEGORY_DETAILS:
      state.details = action.data
      state.loading = false
      return state
    case TYPES.CATEGORY_CREATE:
      state.loading = false
      return state
    case TYPES.CATEGORY_EDIT:
      state.edited = action.data
      return state
    default:
      return state
  }
}

export default reducer
