import TYPES from '../types'

const INITIAL_STATE = {
  loading: false,
  clients: [],
  details: {},
  edited: {}
}

const reducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case TYPES.CLIENT_LOADING:
      state.loading = action.status
      return state
    case TYPES.CLIENT_LIST:
      state.loading = false
      return {  
        ...state,
        suppliers: [...action.data],// new suppliers array
       }
    case TYPES.CLIENT_DETAILS:
      state.details = action.data
      state.loading = false
      return state
    case TYPES.CLIENT_EDIT:
      state.edited = action.data
      return state
    case TYPES.CLIENT_CREATE:
      state.loading = false
      return state
    default:
      return state
  }
}

export default reducer