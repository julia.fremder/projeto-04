import TYPES from '../types'
import { addClient, getClientById } from '../../services/client.service'
import { toastr } from 'react-redux-toastr'

export const addNewClient = (form) => {
  return async (dispatch) => {
    const config = {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    }
    dispatch({
      type: TYPES.CLIENT_LOADING,
      status: true
    })
    try {
      const formNewClient = new FormData()
      Object.keys(form).map((itemForm) =>
        formNewClient.append(itemForm, form[itemForm])
      )
      const result = await addClient(formNewClient, config)
      dispatch({
        type: TYPES.CLIENT_LOADING,
        status: false
      })
      toastr.success('Cadastro', 'Bem vindo ao Oficinadvisor')
      // dispatch(getCategoriesList())
    } catch (error) {
      dispatch({ type: TYPES.CLIENT_LOADING, status: false })
      throw 'erro'
    }
  }
}

export const getDataClientById = (id) => {
  return async (dispatch) => {
    dispatch({
      type: TYPES.CLIENT_LOADING,
      status: true
    })
    try {
      const result = await getClientById(id)
      dispatch({ type: TYPES.CLIENT_DETAILS, data: result.data })
    } catch (error) {
      dispatch({ type: TYPES.CLIENT_LOADING, status: false })
    }
  }
}
