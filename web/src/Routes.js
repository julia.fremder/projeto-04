/* eslint-disable multiline-ternary */
/* eslint-disable jsx-quotes */
import React from 'react'
import { Router, Redirect } from '@reach/router'

// ## STORAGE
import { isAuthenticated } from './config/storage'

// ## VIEWS
import SearchHome from './views/SearchHome'
import Dashboard from './views/Dashboard'

const PrivateRoute = ({ component: Component, ...rest }) => {
  if (!isAuthenticated()) {
    return <Redirect to="/" noThrow />
  }
  return <Component {...rest} />
}

const Routes = () => {
  return (
    <Router id="route-main">
      <SearchHome path="/" />
      <PrivateRoute component={Dashboard} path="/dashboard" />
    </Router>
  )
}

export default Routes
