import http from '../config/http'


export const getClientById = (client_id) => http.get(`/client/${client_id}`)

export const addClient = (data, config = {}) => http.post('/client', data, config)

export const updateClient = (client_id, data, config = {}) => http.put(`/client/${client_id}`, data, config)

