import http from '../config/http'

export const getSuppliers = () => http.get('/supplier')

export const getSupplierById = (supplier_id) => http.get(`/supplier/${supplier_id}`)

export const addSupplier = (data, config = {}) => http.post('/supplier', data, config)

export const updateSupplier = (supplier_id, data, config = {}) => http.put(`/supplier/${supplier_id}`, data, config)

export const deleteSupplier = (supplier_id) => http.delete(`/supplier/${supplier_id}`)

export const approveSupplier = (supplier_id) => http.put(`/supplier/${supplier_id}/approve`)

export const refuseSupplier = (supplier_id) => http.put(`/supplier/${supplier_id}/refuse`)