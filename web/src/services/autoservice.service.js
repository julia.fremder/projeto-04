import http from '../config/http'
import { parsedToQuery } from '../util/helpers'

export const getAutoservices = (query) => {
  const q = query ? `?${parsedToQuery(query)}` : ''
  return http.get(`/autoservice/${q}`)
}

// export const getAutoserviceById = (supplier_id, autoservice_id) => http.get(`/supplier/${supplier_id}`)

export const addAutoservice = (data, supplier_id, config = {}) =>
  http.post(`/supplier/${supplier_id}/autoservices`, data, config)

export const deleteAutoservices = (supplier_id, autoservice_id) =>
  http.delete(`/supplier/${supplier_id}/autoservices/${autoservice_id}`)

export const getAll = (query) => {
  const q = query ? parsedToQuery(query) : ''
  return http.get(`${baseUrl}?${q}`)
}

export const remove = (id) => http.delete(`${baseUrl}/${id}`)

export const create = (fornecedorId, data, config = {}) =>
  http.post(`fornecedor/${fornecedorId}/produto`, data, config)
