import http from '../config/http'

export const getCategories = () => http.get('/category')

export const getCategoryById = (category_id) => http.get(`/category/${category_id}`)

export const addCategory = (data, config = {}) => http.post('/category', data, config)

export const updateCategory = (category_id, data, config = {}) => http.put(`/category/${category_id}`, data, config)

export const deleteCategory = (category_id) => http.delete(`/category/${category_id}`)