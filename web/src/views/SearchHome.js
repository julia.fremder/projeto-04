import React from 'react'
import BannerImage from '../components/BannerImage'
import BannerCards from '../components/BannerCards'
import LayoutHome from '../components/LayoutHome'
import { GlobalContext } from '../GlobalContext'

const SearchHome = () => {
  const global = React.useContext(GlobalContext)
  const { detailComponent, modal } = global
  return (
    <>
      {detailComponent(modal.section)}
      <LayoutHome>
        <BannerImage />
        <BannerCards />
      </LayoutHome>
    </>
  )
}

export default SearchHome
