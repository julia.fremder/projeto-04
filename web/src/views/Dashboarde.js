import React from 'react'

//----------COMPONENTS--------------------//
import Manager from '../components/Manager'
import DashSupplier from '../components/DashSupplier'
import Client from '../components/ChildrenDashClient'
import LayoutDash from '../components/LayoutDash'
import { GlobalContext } from '../GlobalContext.js'

const Dashboard = () => {
  const global = React.useContext(GlobalContext)
  const { user } = global
  return (
    <LayoutDash>
      {user && user.kind === 'Manager' && <Manager className="paper" />}
      {user && user.kind === 'Supplier' && <DashSupplier />}
      {user && user.kind === 'Client' && <Client />}
    </LayoutDash>
  )
}

export default Dashboard
