import React from 'react'
import { Container, Row } from 'reactstrap'
import wallpaper from '../assets/image/wallpaper.jpg'

//----------COMPONENTS--------------------//
import LayoutDash from '../components/LayoutDash'
import { GlobalContext } from '../GlobalContext.js'
import styled from 'styled-components'

const Dashboard = () => {
  const global = React.useContext(GlobalContext)
  const { modal, detailComponent } = global

  return (
    <LayoutDash>
        {detailComponent(modal.section)}
    </LayoutDash>
  )
}

export default Dashboard

