import axios from 'axios' // import da dependencia
import { getToken, removeToken } from './storage'
import { navigate } from '@reach/router'
import { toastr } from 'react-redux-toastr'

// definindo a url da api
const urlApi = process.env.REACT_APP_API + process.env.REACT_APP_VERSION

// criando um client http através do AXIOS
const http = axios.create({
  baseURL: urlApi
})

// Definindo o header padrão da aplicação
http.defaults.headers['content-type'] = 'application/json'
if (getToken()) {
  http.defaults.headers.token = getToken()
}

http.interceptors.response.use(
  (response) => response,
  (error) => {
    switch (error.response.status) {
      case 401:
        if(error.response.data){
          toastr.error('Erro de autenticação', `${error.response.data.message[0]}`)
        }
        if (getToken()) {
          store.dispatch(logoutAction())
          navigate('/')
        }
        return Promise.reject(error)
      case 403:
          if(error.response.data){
            toastr.error('Erro de autorização', `${error.response.data.message[0]}`)
          }
          if (getToken()) {
            store.dispatch(logoutAction())
            navigate('/')
          }
          return Promise.reject(error)
      case 400:
        console.log(error.response)
        if(error.response.data && error.response.data.message.length > 0 ){
          toastr.error('Algo deu errado', error.response.data.message.join(','))
        }
        return Promise.reject(error)
      default:
        return Promise.reject(error)
    }
  }
)

export default http
